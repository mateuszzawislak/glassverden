INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('COLOR_SHINY', 'COLOR', 0, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('COLOR_MATT', 'COLOR', 1, TRUE, 0);

UPDATE project_parameter_options SET parameter_option_code = 'COLOR_SHINY' WHERE parameter_option_code = 'COLOR_SATIN';
UPDATE project_parameter_options SET parameter_option_code = 'COLOR_MATT' WHERE parameter_option_code = 'COLOR_CHROME';

DELETE FROM parameter_option WHERE code = 'COLOR_SATIN';
DELETE FROM parameter_option WHERE code = 'COLOR_CHROME';