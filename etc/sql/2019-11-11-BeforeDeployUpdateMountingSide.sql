INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_120', 'MOUNTING', 1, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_135', 'MOUNTING', 2, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_150', 'MOUNTING', 3, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_165', 'MOUNTING', 4, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_180', 'MOUNTING', 5, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_195', 'MOUNTING', 6, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_210', 'MOUNTING', 7, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_225', 'MOUNTING', 8, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_240', 'MOUNTING', 9, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_255', 'MOUNTING', 10, TRUE, 0);

UPDATE project_parameter_options SET parameter_option_code = 'MOUNTING_SIDE_195' WHERE parameter_option_code  = 'MOUNTING_SIDE';

DELETE FROM parameter_option WHERE code = 'MOUNTING_SIDE';