ALTER TABLE contact ADD COLUMN phone varchar(255);
UPDATE contact SET phone = '9999';
ALTER TABLE contact ALTER COLUMN phone SET NOT NULL;