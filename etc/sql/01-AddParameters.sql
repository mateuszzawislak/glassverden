INSERT INTO parameter (code, rank, active)
VALUES ('HANDRAIL', 0, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('HANDRAIL_NO_RAIL', 'HANDRAIL', 0, TRUE, -0.1125);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('HANDRAIL_WITH_RAIL', 'HANDRAIL', 1, TRUE,  0);


INSERT INTO parameter (code, rank, active)
VALUES ('GLASS_TYPE', 1, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('GLASS_NORMAL', 'GLASS_TYPE', 0, TRUE, 0);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('GLASS_TEMPERED', 'GLASS_TYPE', 1, TRUE, 0.0875);


INSERT INTO parameter (code, rank, active)
VALUES ('GLASS_COLOR', 2, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('GLASS_COLOR_TRANSPARENT', 'GLASS_COLOR', 0, TRUE, 0);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('GLASS_COLOR_GREY', 'GLASS_COLOR', 1, TRUE, 0.0875);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('GLASS_COLOR_BROWN', 'GLASS_COLOR', 2, TRUE, 0.0875);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('GLASS_COLOR_WHITE', 'GLASS_COLOR', 3, TRUE, 0.0875);


INSERT INTO parameter (code, rank, active)
VALUES ('PROFILE', 3, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('PROFILE_ROUND', 'PROFILE', 0, TRUE, 0);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('PROFILE_SQUARE', 'PROFILE', 1, TRUE, 0.200);


INSERT INTO parameter (code, rank, active)
VALUES ('COLOR', 4, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('COLOR_SHINY', 'COLOR', 0, TRUE, 0);
--TODO COLOR_CHROME check price
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('COLOR_MATT', 'COLOR', 1, TRUE, 0);


INSERT INTO parameter (code, rank, active)
VALUES ('MOUNTING', 5, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_GROUND', 'MOUNTING', 0, TRUE, 0);
--TODO MOUNTING_SIDE check price
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_120', 'MOUNTING', 1, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_135', 'MOUNTING', 2, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_150', 'MOUNTING', 3, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_165', 'MOUNTING', 4, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_180', 'MOUNTING', 5, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_195', 'MOUNTING', 6, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_210', 'MOUNTING', 7, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_225', 'MOUNTING', 8, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_240', 'MOUNTING', 9, TRUE, 0);
INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_SIDE_255', 'MOUNTING', 10, TRUE, 0);


INSERT INTO parameter (code, rank, active)
VALUES ('MOUNTING_START', 6, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_START_NOTHING', 'MOUNTING_START', 0, TRUE, 0);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_START_HANDRAIL', 'MOUNTING_START', 1, TRUE, 0);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_START_HANDRAIL_AND_GLASS', 'MOUNTING_START', 2, TRUE, 0);


INSERT INTO parameter (code, rank, active)
VALUES ('MOUNTING_END', 7, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_END_NOTHING', 'MOUNTING_END', 0, TRUE, 0);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_END_HANDRAIL', 'MOUNTING_END', 1, TRUE, 0);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('MOUNTING_END_HANDRAIL_AND_GLASS', 'MOUNTING_END', 2, TRUE, 0);


INSERT INTO parameter (code, rank, active)
VALUES ('HEIGHT', 8, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('HEIGHT_900', 'HEIGHT', 0, TRUE, -0.05625);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('HEIGHT_1000', 'HEIGHT', 1, TRUE, 0);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('HEIGHT_1200', 'HEIGHT', 2, TRUE, 0.1125);


INSERT INTO parameter (code, rank, active)
VALUES ('STEEL_CLASS', 9, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('STEEL_CLASS_AISI316', 'STEEL_CLASS', 0, TRUE, 0);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('STEEL_CLASS_AISI304', 'STEEL_CLASS', 1, TRUE, -0.175);


--TODO GLASS_THICKNESS check price
INSERT INTO parameter (code, rank, active)
VALUES ('GLASS_THICKNESS', 10, TRUE);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('GLASS_THICKNESS_44_2', 'GLASS_THICKNESS', 0, TRUE, 0);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('GLASS_THICKNESS_55_2', 'GLASS_THICKNESS', 1, TRUE, 0.0475);

INSERT INTO parameter_option (code, parameter_code, rank, active, price_weight)
VALUES ('GLASS_THICKNESS_66_2', 'GLASS_THICKNESS', 2, TRUE, 0.0875);