module.exports = function (api) {
  api.cache(true);

  return {
    presets: [
      ['@babel/preset-env', {
        targets: {
          chrome: 41,
        },
        useBuiltIns: 'usage',
        corejs: { version: '3' }
      }]],
    plugins: [
      '@babel/plugin-syntax-dynamic-import',
    ],
  };
};