# Glassverden

## **Backend**
## Used tools/environments (inter alia):

* [Gradle] (any version required),
* [Thymeleaf],
* [Spring Boot]:
	* Data JPA
	* Web
* [PostgreSQL] (version 11.4 required)

## Launching project

* Build project
```sh
./gradlew clean build
./gradlew cleaneclipse eclipse -- for Eclipse
./gradlew idea -- for IntelliJ IDEA
```
* Import project to IDE (e.g. Eclipse).
* Run postgres terminal
```sh
sudo -u postgres psql postgres
```
* Execute script /glassverden/etc/sql/00-CreateDatabaseAndUser.sql to create database and user. You should also launch different Add* scripts to create necessary data.
* Set formatter in IDE and set autoformat edited lines on save.

## ... and that's it! Check it out:

```sh
http://localhost:8080/
```

## Production execution
```sh
/usr/bin/java -Dspring.profiles.active=prod -DAPP_DIR=/opt/glassverden -jar /opt/glassverden/glassverden.jar
```

## **Frontend**
## Used tools/environments

https://nodejs.org/en/download/

* Node.js [building environment] (required version >= 8.0.0) 
* Webpack [bundler] (required version >= 4.0.0)
* Babel.js [transpiler] (required version >= 7.0.0)
* Vue.js
* Sass

## Launching project

* Webpack must be installed as a global package
```sh
npm webpack webpack-cli -g
```

* Install project dependendencies
```sh
npm install
```

* Run development task (watching for changes)
```sh
npm run dev
```

* Run build task (output is in ./dist directory)
```sh
npm run prod
```

## Deploy project

### Ansible instalation

```sh
sudo apt-add-repository -y ppa:ansible/ansible sudo apt-get update sudo apt-get install -y ansible
```

### Ansible hosts

```sh
vim /etc/ansible/hosts

```

```sh
[dev]
<ip ex. 127.0.0.1>

[prod]
<ip ex. 127.0.0.1>

```

### Deploy

Update application-prod.properties before deploy:

* Database password
* SMTP password
* SSL key store password


```sh
ansible-playbook scripts/deploy/main.yml --extra-vars "target=prod" -u root --ask-pass

```

### SSL

* Download: CA.crt CSR.txt privateKey.pem
* Generate p12 file:

```sh
openssl pkcs12 -export -out home-glassverden.p12 -inkey privateKey.pem -in certificate.crt -certfile CA.crt
```

## Gallery

![configurator](src/main/images/configurator.jpg)