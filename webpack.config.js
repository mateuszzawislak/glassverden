const os = require('os');
const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');

const getIp = () => {
  const ifaces = os.networkInterfaces();
  const addresses = Object.values(ifaces).find((iface) => {
    return iface.find(details => details.family === 'IPv4' && !details.internal);
  });

  if (addresses) {
    const ip = addresses.find(address => address.family === 'IPv4');
    if (ip) {
      return ip.address;
    }
  }

  return '0.0.0.0';
};

const hostIp = getIp();
const isDevMode = process.env.NODE_ENV === 'development';
const isDevServer = process.env.WEBPACK_DEV_SERVER;
const srcPath = 'src/main/';
const licenseCsg = fs.readFileSync(path.resolve(__dirname, srcPath, 'javascript/libs/csg/LICENSE'), 'utf-8');

module.exports = {
  mode: isDevMode ? 'development' : 'production',
  entry: [
    path.resolve(__dirname, srcPath, 'javascript/main.js'),
    path.resolve(__dirname, srcPath, 'styles/main.scss'),
  ],
  output: {
    path: path.resolve(__dirname, './src/main/resources/static/'),
    filename: 'javascript/[name].js',
    publicPath: '/static/',
    chunkFilename: 'javascript/[id].chunk.js'
  },
  resolve: {
    extensions: ['.js', '.vue'],
    alias: {
      '@': path.resolve(__dirname, srcPath, 'javascript'),
      '@views': path.resolve(__dirname, srcPath, 'javascript/views'),
      '@components': path.resolve(__dirname, srcPath, 'javascript/components'),
      '@mixins': path.resolve(__dirname, srcPath, 'javascript/mixins'),
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'src'),
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        include: path.resolve(__dirname, 'src'),
      },
      {
        test: /\.scss$/,
        use: [
          isDevServer ? 'vue-style-loader' : MiniCSSExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: isDevMode,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: isDevMode,
            },
          },
        ]
      },
      {
        test: /\.(png|jpe?g|gif)(\?.*)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]',
            }
          }
        ]
      },
      {
        test: /\.(woff2?|eot|ttf|otf|svg)(\?.*)?$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'fonts/[name].[ext]',
            }
          }
        ]
      },
    ],
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: isDevMode,
      }),
      new OptimizeCSSAssetsPlugin(),
    ],
  },
  plugins: [
    new webpack.BannerPlugin({
      banner: licenseCsg,
    }),
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
    }),
    new VueLoaderPlugin(),
    new MiniCSSExtractPlugin({
      filename: 'styles/[name].css',
      chunkFilename: 'styles/[id].css'
    }),
    isDevServer && new CopyPlugin([
      { from: path.resolve(__dirname, srcPath, '*.html'), to: './[name].html' },
    ]),
    new CopyPlugin([
      { from: path.resolve(__dirname, srcPath, 'images/*'), to: './images/[name].[ext]' },
    ]),
    isDevServer && new webpack.EnvironmentPlugin({
      DEV_SERVER: true,
    }),
    isDevServer && new webpack.HotModuleReplacementPlugin(),
    new FriendlyErrorsWebpackPlugin(),
    isDevMode && !isDevServer && new LiveReloadPlugin({
      delay: 2000, // [ms]
    }),
  ].filter(Boolean),
  watch: isDevMode,
  devtool: isDevMode ? 'source-map' : 'none',
  devServer: {
    historyApiFallback: {
      index: path.resolve(__dirname, srcPath, 'index.html'),
    },
    compress: true,
    open: true,
    overlay: true,
    port: 8000,
    hot: true,
    // host: !process.env.DEV_HOST && hostIp,
    stats: {
      normal: true
    }
  }
};
