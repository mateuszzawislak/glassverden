export default {
  GLASS_NORMAL: 'normal',
  GLASS_TEMPERED: 'tempered',

  HEIGHT_900: 'height900',
  HEIGHT_1000: 'height1000',
  HEIGHT_1200: 'height1200',

  MOUNTING_GROUND: 'mountingGround',
  MOUNTING_SIDE_120: 'mountingSide120',
  MOUNTING_SIDE_135: 'mountingSide135',
  MOUNTING_SIDE_150: 'mountingSide150',
  MOUNTING_SIDE_165: 'mountingSide165',
  MOUNTING_SIDE_180: 'mountingSide180',
  MOUNTING_SIDE_195: 'mountingSide195',
  MOUNTING_SIDE_210: 'mountingSide210',
  MOUNTING_SIDE_225: 'mountingSide225',
  MOUNTING_SIDE_240: 'mountingSide240',
  MOUNTING_SIDE_255: 'mountingSide255',

  STEEL_CLASS_AISI316: 'AISI316',
  STEEL_CLASS_AISI304: 'AISI304'
};