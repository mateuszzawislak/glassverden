export const DimensionProp = {
  ANGLE: 'angle',
  LENGTH: 'length',
  SEGMENTS_NUMBER: 'segmentsNumber',
}

export const MinDimensionValue = {
  [DimensionProp.ANGLE]: 0,
  [DimensionProp.LENGTH]: 0,
  [DimensionProp.SEGMENTS_NUMBER]: 1,
};

export const NewOptionValue = {
  [DimensionProp.ANGLE]: 90,
  [DimensionProp.LENGTH]: 3000,
  [DimensionProp.SEGMENTS_NUMBER]: 3,
}