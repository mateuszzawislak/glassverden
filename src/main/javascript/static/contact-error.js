export default {
  NOT_BLANK: 'notBlank',
  MAX_SIZE_EXCEEDED: 'maxSizeExceeded',
  NOT_AN_EMAIL: 'notAnEmail',
};