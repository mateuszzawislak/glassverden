export default {
  EDGE_TOO_LONG: 'edgeTooLong',
  EDGE_TOO_SHORT: 'edgeTooShort',
  INVALID_PROJECT_STATUS: 'invalidProjectStatus',
  MAX_359: 'maxAngle',
  MIN_1: 'minValue',
  ONE_SEGMENT_REQUIRED: 'oneSegmentRequired',
  SEGMENT_TOO_LONG: 'segmentTooLong',
  SEGMENT_TOO_SHORT: 'segmentTooShort',
};