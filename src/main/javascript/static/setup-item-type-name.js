export default {
  COLOR_SHINY: 'COLOR_SHINY',
  COLOR_MATT: 'COLOR_MATT',

  GLASS_COLOR_TRANSPARENT: 'GLASS_COLOR_TRANSPARENT',
  GLASS_COLOR_GREY: 'GLASS_COLOR_GREY',
  GLASS_COLOR_BROWN: 'GLASS_COLOR_BROWN',
  GLASS_COLOR_WHITE: 'GLASS_COLOR_WHITE',

  GLASS_NORMAL: 'GLASS_NORMAL',
  GLASS_TEMPERED: 'GLASS_TEMPERED',

  GLASS_THICKNESS_44_2: 'GLASS_THICKNESS_44_2',
  GLASS_THICKNESS_55_2: 'GLASS_THICKNESS_55_2',
  GLASS_THICKNESS_66_2: 'GLASS_THICKNESS_66_2',

  HEIGHT_900: 'HEIGHT_900',
  HEIGHT_1000: 'HEIGHT_1000',
  HEIGHT_1200: 'HEIGHT_1200',

  HANDRAIL_NO_RAIL: 'HANDRAIL_NO_RAIL',
  HANDRAIL_WITH_RAIL: 'HANDRAIL_WITH_RAIL',

  MOUNTING_GROUND: 'MOUNTING_GROUND',
  MOUNTING_SIDE_120: 'MOUNTING_SIDE_120',
  MOUNTING_SIDE_135: 'MOUNTING_SIDE_135',
  MOUNTING_SIDE_150: 'MOUNTING_SIDE_150',
  MOUNTING_SIDE_165: 'MOUNTING_SIDE_165',
  MOUNTING_SIDE_180: 'MOUNTING_SIDE_180',
  MOUNTING_SIDE_195: 'MOUNTING_SIDE_195',
  MOUNTING_SIDE_210: 'MOUNTING_SIDE_210',
  MOUNTING_SIDE_225: 'MOUNTING_SIDE_225',
  MOUNTING_SIDE_240: 'MOUNTING_SIDE_240',
  MOUNTING_SIDE_255: 'MOUNTING_SIDE_255',

  MOUNTING_END_NOTHING: 'MOUNTING_END_NOTHING',
  MOUNTING_END_HANDRAIL: 'MOUNTING_END_HANDRAIL',
  MOUNTING_END_HANDRAIL_AND_GLASS: 'MOUNTING_END_HANDRAIL_AND_GLASS',

  MOUNTING_START_NOTHING: 'MOUNTING_START_NOTHING',
  MOUNTING_START_HANDRAIL: 'MOUNTING_START_HANDRAIL',
  MOUNTING_START_HANDRAIL_AND_GLASS: 'MOUNTING_START_HANDRAIL_AND_GLASS',

  PROFILE_ROUND: 'PROFILE_ROUND',
  PROFILE_SQUARE: 'PROFILE_SQUARE',

  RAILING_WITH_BAR: 'RAILING_WITH_BAR',
  RAILING_WITH_POSTS: 'RAILING_WITH_POSTS',
  RAILING_WITH_ROTULES: 'RAILING_WITH_ROTULES',

  STEEL_CLASS_AISI316: 'STEEL_CLASS_AISI316',
  STEEL_CLASS_AISI304: 'STEEL_CLASS_AISI304'
};