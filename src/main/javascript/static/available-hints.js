import SetupItemTypeName from './setup-item-type-name';
import SetupItemTypeLabel from './setup-item-type-label';

export default {
  [SetupItemTypeName.GLASS_NORMAL]: SetupItemTypeLabel.GLASS_NORMAL,
  [SetupItemTypeName.GLASS_TEMPERED]: SetupItemTypeLabel.GLASS_TEMPERED,
  [SetupItemTypeName.HEIGHT_900]: SetupItemTypeLabel.HEIGHT_900,
  [SetupItemTypeName.HEIGHT_1000]: SetupItemTypeLabel.HEIGHT_1000,
  [SetupItemTypeName.HEIGHT_1200]: SetupItemTypeLabel.HEIGHT_1200,
  [SetupItemTypeName.MOUNTING_GROUND]: SetupItemTypeLabel.MOUNTING_GROUND,
  [SetupItemTypeName.MOUNTING_SIDE_120]: SetupItemTypeLabel.MOUNTING_SIDE_120,
  [SetupItemTypeName.MOUNTING_SIDE_135]: SetupItemTypeLabel.MOUNTING_SIDE_135,
  [SetupItemTypeName.MOUNTING_SIDE_150]: SetupItemTypeLabel.MOUNTING_SIDE_150,
  [SetupItemTypeName.MOUNTING_SIDE_165]: SetupItemTypeLabel.MOUNTING_SIDE_165,
  [SetupItemTypeName.MOUNTING_SIDE_180]: SetupItemTypeLabel.MOUNTING_SIDE_180,
  [SetupItemTypeName.MOUNTING_SIDE_195]: SetupItemTypeLabel.MOUNTING_SIDE_195,
  [SetupItemTypeName.MOUNTING_SIDE_210]: SetupItemTypeLabel.MOUNTING_SIDE_210,
  [SetupItemTypeName.MOUNTING_SIDE_225]: SetupItemTypeLabel.MOUNTING_SIDE_225,
  [SetupItemTypeName.MOUNTING_SIDE_240]: SetupItemTypeLabel.MOUNTING_SIDE_240,
  [SetupItemTypeName.MOUNTING_SIDE_255]: SetupItemTypeLabel.MOUNTING_SIDE_255,
  [SetupItemTypeName.STEEL_CLASS_AISI316]: SetupItemTypeLabel.STEEL_CLASS_AISI316,
  [SetupItemTypeName.STEEL_CLASS_AISI304]: SetupItemTypeLabel.STEEL_CLASS_AISI304,
};