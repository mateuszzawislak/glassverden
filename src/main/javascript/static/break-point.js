const BreakPoint = {
  XS: 576,
  SM: 768,
  MD: 992,
  XL: 1200,
  XXL: 1440,
};

export const breakPointValues = Object.values(BreakPoint).sort();
export default BreakPoint;