export default {
  ABOUT: 'about',
  CONTACT: 'contact',
  HOME: 'home',
  PROJECTS: 'projects',
  RAILING_CONFIGURATOR: 'railing-configurator',
  SUMMARY: 'summary',
};