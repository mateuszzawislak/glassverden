import Vue from 'vue';
import Router from 'vue-router';
import routes from '@/routes';
import { languageManager } from '@/libs';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: '/' || process.env.BASE_URL,
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  }
});

router.beforeEach((to, from, next) => {
  const modifiedQuery = languageManager.parseUrl(to);
  let newRoute;

  if (modifiedQuery) {
    newRoute = { ...to, query: modifiedQuery, replace: true };
  }

  next(newRoute);
});

export default router;