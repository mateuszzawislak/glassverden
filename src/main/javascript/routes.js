import { ContactPage, Home, RailingConfigurator, ProjectsPage, AboutPage, SummaryPage } from '@views';
import { RouteName } from '@/static';

export default [
  {
    path: '/',
    name: RouteName.HOME,
    component: Home,
  },
  {
    path: '/about',
    name: RouteName.ABOUT,
    component: AboutPage,
  },
  {
    path: '/configurator',
    name: RouteName.RAILING_CONFIGURATOR,
    component: RailingConfigurator,
  },
  {
    path: '/contact',
    name: RouteName.CONTACT,
    component: ContactPage,
  },
  {
    path: '/projects',
    name: RouteName.PROJECTS,
    component: ProjectsPage,
  },
  {
    path: '/summary',
    name: RouteName.SUMMARY,
    component: SummaryPage,
  },
];