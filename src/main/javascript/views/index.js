import AboutPage from '@/views/about/index.vue';
import ContactPage from '@/views/contact/index.vue';
import Home from '@views/home/index.vue';
import ProjectsPage from '@/views/projects/index.vue';
import RailingConfigurator from '@/views/railing-configurator/index.vue';
import SummaryPage from '@/views/summary/index.vue';

export { AboutPage };
export { ContactPage };
export { Home };
export { ProjectsPage };
export { RailingConfigurator };
export { SummaryPage };