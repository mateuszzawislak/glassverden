import * as THREE from 'three';
import { SetupItemName, SetupItemTypeName } from '@/static';
import { disposeObjects } from '@/utils';

const DISTANCE_FROM_TOP_WITHOUT_HANDRAIL = 0;
const DISTANCE_FROM_TOP_WITH_HANDRAIL = 0.03;

export default {
  data() {
    return {
      glasses: [],
      color: null,
      glassParams: {
        color: this.setup[SetupItemName.GLASS_COLOR].optionId,
        distanceFromTop: undefined, //distance between upper glass edge and end of column
        distanceFromBottom: 0.05, //distance between lower glass edge and platform level
        horizontalDistance: 0.02, //distance between side edge of glass and edge of column
        thickness: this.setup[SetupItemName.GLASS_THICKNESS].optionId,
      }
    };
  },
  watch: {
    [`setup.${SetupItemName.GLASS_COLOR}.optionId`](color) {
      this.glassParams.color = color;
      this.redrawGlasses();
    },
    [`setup.${SetupItemName.GLASS_THICKNESS}.optionId`](thickness) {
      this.glassParams.thickness = thickness;
      this.redrawGlasses();
    },
    [`setup.${SetupItemName.HEIGHT}.optionId`]() {
      this.redrawGlasses();
    },
    [`setup.${SetupItemName.MOUNTING}.optionId`]() {
      this.redrawGlasses();
    }
  },
  beforeDestroy() {
    this.removeGlasses();
  },
  methods: {
    redrawGlasses() {
      this.removeGlasses();
      this.createGlasses();
      this.renderer.render(this.scene, this.camera);
    },

    createGlasses() {
      let glassesCoordinates = this.calculateGlassesCoordinates(this.mountingsCoordinates);
      glassesCoordinates.forEach(glassCoordinates => {
        const glass = this.createGlass();
        this.positionGlassInScene(glass, glassCoordinates);
      });
    },

    calculateGlassesCoordinates(mountingsCoordinates) {
      let prevCorner;
      this.glassParams.distanceFromTop = this.commonParams.handrail === SetupItemTypeName.HANDRAIL_NO_RAIL ?
        DISTANCE_FROM_TOP_WITHOUT_HANDRAIL : DISTANCE_FROM_TOP_WITH_HANDRAIL;

      return mountingsCoordinates.reduce((acc, currCorner, i) => {
        if (!i) {
          prevCorner = currCorner;
          return [];
        }

        const width = prevCorner.distanceTo(currCorner) - 2 * (this.glassParams.horizontalDistance + this.columnParams.radius);
        const height = this.getHeight() - this.glassParams.distanceFromTop - this.glassParams.distanceFromBottom;

        const position = new THREE.Vector3(
          (prevCorner.x + currCorner.x) / 2,
          (prevCorner.y + currCorner.y) / 2 + (this.glassParams.distanceFromBottom - this.glassParams.distanceFromTop) / 2,
          (prevCorner.z + currCorner.z) / 2);
        let rotation = this.calculateRotationBetweenTwoPoints(currCorner, prevCorner);

        prevCorner = currCorner;
        return [...acc, { width, height, position, rotation }];
      }, []);
    },

    createGlass() {
      const geometry = new THREE.BoxBufferGeometry(1, 1, this.getGlassDepth());
      let glass = new THREE.Mesh(geometry, this.getGlassMaterial());
      glass.castShadow = true;
      glass.renderOrder = 10;

      return glass;
    },

    getGlassDepth() {
      switch(this.glassParams.thickness) {
        case SetupItemTypeName.GLASS_THICKNESS_44_2:
          return 0.0087;
        case SetupItemTypeName.GLASS_THICKNESS_55_2:
          return 0.0115;
        case SetupItemTypeName.GLASS_THICKNESS_66_2:
        default:
          return 0.0127;
      }
  },

    getGlassMaterial() {
      let color;
      let opacity;

      switch(this.glassParams.color) {
        case SetupItemTypeName.GLASS_COLOR_WHITE:
          color = '#fff';
          opacity = 0.8;
          break;
        case SetupItemTypeName.GLASS_COLOR_GREY:
          color = '#000';
          opacity = 0.25;
          break;
        case SetupItemTypeName.GLASS_COLOR_BROWN:
          color = '#522911';
          opacity = 0.2;
          break;
        case SetupItemTypeName.GLASS_COLOR_TRANSPARENT:
        default:
         color = '#fff';
         opacity = 0.2;
         break;
      }

      return new THREE.MeshPhongMaterial({ color, transparent: true, opacity });
    },

    positionGlassInScene(glass, glassCoordinates) {
      glass.scale.setX(glassCoordinates.width);
      glass.scale.setY(glassCoordinates.height);
      glass.rotateOnAxis(new THREE.Vector3(0, 1, 0), glassCoordinates.rotation);
      glass.position.copy(glassCoordinates.position);
      this.scene.add(glass);
      this.glasses.push(glass);
    },

    removeGlasses() {
      this.scene.remove(...this.glasses);
      disposeObjects(this.glasses);
      this.glasses = [];
    },
  },
}