import * as THREE from 'three';
import { disposeObjects } from '@/utils';

export default {
  data() {
    return {
      platform: null,
      platformParams: {
        height: 0.4,
        platformDepthForSingleEdge: 1
      }
    }
  },
  beforeDestroy() {
    this.removePlatform();
  },

  methods: {
    redrawPlatform() {
      this.removePlatform();
      this.createPlatform();
      this.renderer.render(this.scene, this.camera);
    },

    createPlatform() {
      let platformShape = new THREE.Shape();
      let cornersCoordinates = this.cornersCoordinates;
      if (this.isProjectWithSingleEdge()) {
        this.createReflectedAdditionalCorners(cornersCoordinates);
      }
      cornersCoordinates.forEach((coordinates, index) => {
        const x = coordinates.x;
        const z = coordinates.z;
        if (index === 0) {
          platformShape.moveTo(x, z);
        } else {
          platformShape.lineTo(x, z);
        }
      });
      platformShape.closePath();

      const platformGeometry = new THREE.ExtrudeBufferGeometry(platformShape, {
        depth: this.platformParams.height,
        bevelEnabled: false,
      });
      const material = new THREE.MeshStandardMaterial({ color: '#966F33', roughness: 0.7 });
      this.platform = new THREE.Mesh(platformGeometry, material);
      this.platform.rotateOnAxis(new THREE.Vector3(1, 0, 0), Math.PI / 2);
      this.platform.receiveShadow = true;
      this.platform.castShadow = true;
      this.scene.add(this.platform);
    },

    removePlatform() {
      this.scene.remove(this.platform);
      disposeObjects([this.platform]);
      this.platform = null;
    },

    createReflectedAdditionalCorners(coordinates) {
      coordinates.push(coordinates[1].clone().setX(-this.platformParams.platformDepthForSingleEdge));
      coordinates.push(coordinates[0].clone().setX(-this.platformParams.platformDepthForSingleEdge));
    },
  },
}