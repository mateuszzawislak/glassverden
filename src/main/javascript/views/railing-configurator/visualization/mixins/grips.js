import * as THREE from 'three';
import { SetupItemName, SetupItemTypeName } from '@/static';
import { disposeObjects } from '@/utils';

export default {
  data() {
    return {
      grips: [],
      gripParams: {
        type: this.setup[SetupItemName.PROFILE].optionId,
        widthSquare: 0.045, //horizontal part for square profile
        width: 0.033, //horizontal straight part for round profile
        heightSquare: 0.045, //vertical length for square profile
        height: 0.040, //vertical length for round profile
        depth: 0.025, //distance from front side and back side
        distanceFromTop: 0.12, //distance between center of grip and glass upper edge
        distanceFromTopWithoutHandrail: 0.18, //distance between center of grip and glass upper edge in case of no handrail
        distanceFromBottom: 0.12 //distance between center of grip and glass bottom edge
      },
    }
  },
  watch: {
    [`setup.${SetupItemName.PROFILE}.optionId`](profile) {
      this.gripParams.type = profile;
      this.redrawGrips();
    },
    [`setup.${SetupItemName.HEIGHT}.optionId`]() {
      this.redrawGrips();
    },
    [`setup.${SetupItemName.COLOR}.optionId`]() {
      this.redrawGrips();
    },
    [`setup.${SetupItemName.MOUNTING}.optionId`]() {
      this.redrawGrips();
    }
  },
  beforeDestroy() {
    this.removeGrips();
  },
  methods: {
    redrawGrips() {
      this.removeGrips();
      this.createGrips();
      this.renderer.render(this.scene, this.camera);
    },

    createGrips() {
      let gripsCoordinates = this.calculateGripsCoordinates(this.mountingsCoordinates);
      gripsCoordinates.forEach(gripCoordinates => {
        const grip = this.createGrip();
        this.positionGripInScene(grip, gripCoordinates);
      });
    },

    calculateGripsCoordinates(mountingsCoordinates) {
      const distanceFromTop = this.commonParams.handrail !== SetupItemTypeName.HANDRAIL_NO_RAIL ?
        this.gripParams.distanceFromTop : this.gripParams.distanceFromTopWithoutHandrail;
      const moveVectorForTop = new THREE.Vector3(0, distanceFromTop + this.glassParams.distanceFromTop - this.getHeight() / 2, 0);
      const moveVectorForBottom = new THREE.Vector3(0, -this.gripParams.distanceFromBottom - this.glassParams.distanceFromBottom + this.getHeight() / 2, 0);
      let prevCorner;
      return mountingsCoordinates.reduce((acc, currCorner, i) => {
        if (!i) {
          prevCorner = currCorner;
          return [];
        }

        const prevLowerGrip = prevCorner.clone().sub(moveVectorForBottom);
        const prevUpperGrip = prevCorner.clone().sub(moveVectorForTop);
        const currLowerGrip = currCorner.clone().sub(moveVectorForBottom);
        const currUpperGrip = currCorner.clone().sub(moveVectorForTop);

        prevCorner = currCorner;
        return [...acc,
        this.createGripCoordinates(prevLowerGrip, currLowerGrip),
        this.createGripCoordinates(prevUpperGrip, currUpperGrip),
        this.createGripCoordinates(currLowerGrip, prevLowerGrip),
        this.createGripCoordinates(currUpperGrip, prevUpperGrip)
        ];
      }, []);
    },

    createGripCoordinates(gripStartingPoint, gripDirection) {
      return {
        position: gripStartingPoint,
        rotation: this.calculateRotationBetweenTwoPoints(gripStartingPoint, gripDirection)
      };
    },

    createGrip() {
      const { depth: gripDepth, type: gripType } = this.gripParams;
      const gripShape = new THREE.Shape();
      let width = gripType === SetupItemTypeName.PROFILE_ROUND ? this.gripParams.width : this.gripParams.widthSquare;
      width += this.columnParams.radius;
      const gripHeight = gripType === SetupItemTypeName.PROFILE_ROUND ? this.gripParams.height : this.gripParams.heightSquare;

      //Create shape of grips
      gripShape.moveTo(0, gripHeight / 2);
      gripShape.lineTo(width, gripHeight / 2);
      if (gripType === SetupItemTypeName.PROFILE_ROUND) {
        gripShape.ellipse(0, -gripHeight / 2, gripHeight / 2, gripHeight / 2, 0, Math.PI, true, Math.PI / 2);
      }
      gripShape.lineTo(width, -gripHeight / 2);
      gripShape.lineTo(0, -gripHeight / 2);

      //Make depth
      const gripGeometry = new THREE.ExtrudeBufferGeometry(gripShape, {
        depth: gripDepth,
        bevelEnabled: false,
      });
      gripGeometry.translate(0, 0, -gripDepth / 2);
      let grip = new THREE.Mesh(gripGeometry, this.getColor());
      grip.castShadow = true;

      return grip;
    },

    removeGrips() {
      this.scene.remove(...this.grips);
      disposeObjects(this.grips);
      this.grips = [];
    },

    positionGripInScene(grip, gripCoordinates) {
      grip.rotateOnAxis(new THREE.Vector3(0, 1, 0), gripCoordinates.rotation);
      grip.position.copy(gripCoordinates.position);
      this.scene.add(grip);
      this.grips.push(grip);
    },
  },
}