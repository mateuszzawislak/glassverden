import * as THREE from 'three';
import { SetupItemName, SetupItemTypeName } from "@/static";
import { disposeObjects } from '@/utils';
import { CSG } from '@/libs';

export default {
  data() {
    return {
      handrails: [],
      handrailsSupports: [],
      handrailsWallMountings: [],
      handrailsIntersections: [],
      handrailParams: {
        type: this.setup[SetupItemName.HANDRAIL].optionId,
        profile: this.setup[SetupItemName.PROFILE].optionId,
        mountingStart: this.setup[SetupItemName.MOUNTING_START].optionId,
        mountingEnd: this.setup[SetupItemName.MOUNTING_END].optionId,
        supportLength: 0.075, //length of vertical part supporting handrail
        supportRadius: 0.007, //radius of vertical part supporting handrail
        radius: 0.0205, //radius of handrail,
        additionalLengthForSideWithoutMounting: 0.08,
        wallMountingRadius: 0.045,
        wallMountingHeight: 0.015,
      },
    }
  },
  watch: {
    [`setup.${SetupItemName.HANDRAIL}.optionId`](optionId) {
      this.handrailParams.type = optionId;
      this.redrawHandrails();
    },
    [`setup.${SetupItemName.HEIGHT}.optionId`]() {
      this.redrawHandrails();
    },
    [`setup.${SetupItemName.COLOR}.optionId`]() {
      this.redrawHandrails();
    },
    [`setup.${SetupItemName.PROFILE}.optionId`](profile) {
      this.handrailParams.profile = profile;
      this.redrawHandrails();
    },
    [`setup.${SetupItemName.MOUNTING}.optionId`]() {
      this.redrawHandrails();
    },
    [`setup.${SetupItemName.MOUNTING_START}.optionId`](mountingStart) {
      this.handrailParams.mountingStart = mountingStart;
      this.redrawHandrails();
    },
    [`setup.${SetupItemName.MOUNTING_END}.optionId`](mountingEnd) {
      this.handrailParams.mountingEnd = mountingEnd;
      this.redrawHandrails();
    }
  },

  beforeDestroy() {
    this.removeHandrailsAndSupports();
  },

  methods: {
    redrawHandrails() {
      this.removeHandrailsAndSupports();
      this.createHandrails();

      this.renderer.render(this.scene, this.camera);
    },

    createHandrails() {
      if (this.handrailParams.type !== SetupItemTypeName.HANDRAIL_NO_RAIL) {
        const handrailsCoordinates = this.createHandrailsPipes();
        this.createHandrailsSupports();
        this.createHandrailsWallMountings(handrailsCoordinates);
        this.createHandrailsIntersections();
      }
    },

    createHandrailsPipes() {
      let handrailsCoordinates = this.calculateHandrailsCoordinates(this.mountingsCoordinates);
      handrailsCoordinates.forEach(handrailCoordinates => {
        const handrail = this.createHandrail();
        this.positionHandrailInScene(handrail, handrailCoordinates);
      });

      return handrailsCoordinates;
    },

    createHandrailsSupports() {
      this.mountingsCoordinates.forEach(mountingCoordinates => {
        const handrailSupport = this.createHandrailSupport();
        this.positionHandrailSupportInScene(handrailSupport, mountingCoordinates);
      });
    },

    createHandrailsWallMountings(handrailsCoordinates) {
      const { mountingStart, mountingEnd } = this.handrailParams;

      if (mountingStart === SetupItemTypeName.MOUNTING_START_HANDRAIL
        || mountingStart === SetupItemTypeName.MOUNTING_START_HANDRAIL_AND_GLASS) {
        let mountingPosition = this.cornersCoordinates[0].clone().add(this.mountingsCoordinates[0].wallDirection.clone().negate());
        mountingPosition.setY(this.getHandrailYCoordinate());
        let rotation = Math.PI / 2 + handrailsCoordinates[0].rotation;
        this.positionHandrailWallMountingInScene(this.createWallMounting(), mountingPosition, rotation);
      }
      if (mountingEnd === SetupItemTypeName.MOUNTING_END_HANDRAIL
        || mountingEnd === SetupItemTypeName.MOUNTING_END_HANDRAIL_AND_GLASS) {
        let mountingPosition = this.getLastCornerWithMounting().clone().add(this.mountingsCoordinates[this.mountingsCoordinates.length - 1].wallDirection.clone().negate());
        mountingPosition.setY(this.getHandrailYCoordinate());
        let rotation = Math.PI / 2 + handrailsCoordinates[handrailsCoordinates.length - 1].rotation;
        this.positionHandrailWallMountingInScene(this.createWallMounting(), mountingPosition, -rotation);
      }
    },

    getLastCornerWithMounting() {
      if (this.isProjectWithSingleEdge()) {
        return this.cornersCoordinates[1];
      } else {
        return this.cornersCoordinates[this.cornersCoordinates.length - 1];
      }
    },

    calculateHandrailsCoordinates(mountingsCoordinates) {
      let prevCorner;
      let mountingsCount = mountingsCoordinates.length;
      return mountingsCoordinates.reduce((acc, currCorner, i) => {
        if (!i) {
          prevCorner = currCorner;
          return [];
        }

        const isFirstHandrail = i === 1;
        const isLastHandrail = i === mountingsCount - 1;

        let length = prevCorner.distanceTo(currCorner);
        const position = new THREE.Vector3(
          (prevCorner.x + currCorner.x) / 2,
          this.getHandrailYCoordinate(),
          (prevCorner.z + currCorner.z) / 2);
        let rotation = this.calculateRotationBetweenTwoPoints(currCorner, prevCorner);

        const { additionalLength, positionCorrection } = this.getAdditionalHandrailLength(isFirstHandrail, isLastHandrail);
        length += additionalLength;
        position.add(this.calculateShiftVectorBetweenTwoPoints(currCorner, prevCorner, positionCorrection));

        let shiftToPrevMounting = this.calculateShiftVectorBetweenTwoPoints(prevCorner, currCorner);
        let shiftToNextMounting = this.calculateShiftVectorBetweenTwoPoints(currCorner, prevCorner);

        prevCorner = currCorner;
        return [...acc, { length, position, rotation, shiftToPrevMounting, shiftToNextMounting }];
      }, []);
    },

    getHandrailYCoordinate() {
      return this.getHeight() + this.handrailParams.supportLength + this.handrailParams.radius;
    },

    getAdditionalHandrailLength(isFirstHandrail, isLastHandrail) {
      const { mountingStart, mountingEnd, additionalLengthForSideWithoutMounting } = this.handrailParams;
      let additionalLength = 0;
      let positionCorrection = 0;

      if ((isFirstHandrail || this.isProjectWithSingleEdge()) && mountingStart === SetupItemTypeName.MOUNTING_START_NOTHING) {
        additionalLength += additionalLengthForSideWithoutMounting;
        positionCorrection -= additionalLengthForSideWithoutMounting / 2;
      } else if ((isFirstHandrail || this.isProjectWithSingleEdge()) && mountingStart === SetupItemTypeName.MOUNTING_START_HANDRAIL) {
        additionalLength += this.commonParams.lastElementShiftParam;
        positionCorrection -= this.commonParams.lastElementShiftParam / 2;
      }

      if ((isLastHandrail || this.isProjectWithSingleEdge()) && mountingEnd === SetupItemTypeName.MOUNTING_END_NOTHING) {
        additionalLength += additionalLengthForSideWithoutMounting;
        positionCorrection += additionalLengthForSideWithoutMounting / 2;
      } else if ((isLastHandrail || this.isProjectWithSingleEdge()) && mountingEnd === SetupItemTypeName.MOUNTING_END_HANDRAIL) {
        additionalLength += this.commonParams.lastElementShiftParam;
        positionCorrection += this.commonParams.lastElementShiftParam / 2;
      }

      return { additionalLength, positionCorrection };
    },

    createHandrail() {
      const { radius, profile } = this.handrailParams;
      let handrailGeometry;
      if (profile !== SetupItemTypeName.PROFILE_SQUARE) {
        handrailGeometry = new THREE.CylinderBufferGeometry(radius, radius, 1, this.commonParams.radialSegments);
      } else {
        handrailGeometry = new THREE.BoxBufferGeometry(radius * 2, 1, radius * 2);
      }
      let handrail = new THREE.Mesh(handrailGeometry, this.getColor());
      handrail.castShadow = true;

      return handrail;
    },

    createHandrailSupport() {
      const { radius, supportRadius, supportLength, profile } = this.handrailParams;
      let handrailSupportGeometry;
      if (profile !== SetupItemTypeName.PROFILE_SQUARE) {
        handrailSupportGeometry = new THREE.CylinderBufferGeometry(supportRadius, supportRadius, supportLength + radius);
      } else {
        handrailSupportGeometry = new THREE.BoxBufferGeometry(supportRadius * 2, supportLength + radius, supportRadius * 2);
      }
      let handrailSupport = new THREE.Mesh(handrailSupportGeometry, this.getColor());
      handrailSupport.castShadow = true;

      return handrailSupport;
    },

    createWallMounting() {
      const { wallMountingHeight, wallMountingRadius, profile } = this.handrailParams;
      let wallMounting;
      if (profile !== SetupItemTypeName.PROFILE_SQUARE) {
        wallMounting = new THREE.CylinderBufferGeometry(wallMountingRadius,
          wallMountingRadius,
          wallMountingHeight,
          this.commonParams.radialSegments);
      } else {
        wallMounting = new THREE.BoxBufferGeometry(wallMountingRadius * 2,
          wallMountingHeight,
          wallMountingRadius * 2);
      }

      let wallMountingMesh = new THREE.Mesh(wallMounting, this.getColor());
      wallMountingMesh.castShadow = true;

      return wallMountingMesh;
    },

    removeHandrailsAndSupports() {
      this.scene.remove(...this.handrails);
      this.scene.remove(...this.handrailsSupports);
      this.scene.remove(...this.handrailsWallMountings);
      this.scene.remove(...this.handrailsIntersections);
      disposeObjects(this.handrails, this.handrailsSupports, this.handrailsWallMountings, this.handrailsIntersections);
      this.handrails = [];
      this.handrailsSupports = [];
      this.handrailsWallMountings = [];
      this.handrailsIntersections = [];
    },

    positionHandrailInScene(handrail, handrailParams) {
      handrail.scale.setY(handrailParams.length);
      handrail.rotateOnAxis(new THREE.Vector3(0, 0, 1), Math.PI / 2);
      handrail.rotateOnAxis(new THREE.Vector3(1, 0, 0), handrailParams.rotation);
      handrail.position.copy(handrailParams.position);
      handrail.shiftToPrevMounting = handrailParams.shiftToPrevMounting;
      handrail.shiftToNextMounting = handrailParams.shiftToNextMounting;
      this.scene.add(handrail);
      this.handrails.push(handrail);
    },

    createHandrailsIntersections() {
      this.handrails.forEach((handrail, index) => {
        if(index == 0) {
          return;
        }
        let prevHandrail = this.handrails[index-1];
        let clonedPrev = prevHandrail.clone();
        clonedPrev.position.add(prevHandrail.shiftToNextMounting);
        clonedPrev.updateMatrix();

        let clonedCurr = handrail.clone();
        clonedCurr.position.add(handrail.shiftToPrevMounting);
        clonedCurr.updateMatrix();

        let intersectedPart = this.intersectTwoHandrails(clonedPrev, clonedCurr);

        this.scene.add(intersectedPart);
        this.handrailsIntersections.push(intersectedPart);
      })
    },

    intersectTwoHandrails(handrailA, handrailB) {
      let bspA = CSG.fromMesh(handrailA);
      let bspB = CSG.fromMesh(handrailB);
      let bspC = bspA['intersect'](bspB);
      let result = CSG.toMesh(bspC, handrailA.matrix);
      result.material = handrailA.material;
      result.castShadow = true;
      return result;
    },

    positionHandrailSupportInScene(handrailSupport, cornerCoordinates) {
      if (cornerCoordinates.wallMount) {
        disposeObjects([handrailSupport]);
        return;
      }
      let supportPosition = cornerCoordinates.clone().setY(this.getHeight() + (this.handrailParams.supportLength + this.handrailParams.radius) / 2);
      handrailSupport.position.copy(supportPosition);
      this.scene.add(handrailSupport);
      this.handrailsSupports.push(handrailSupport);
    },

    positionHandrailWallMountingInScene(handrailWallMounting, mountingCoordinates, rotation) {
      handrailWallMounting.rotateOnAxis(new THREE.Vector3(1, 0, 0), Math.PI / 2);
      handrailWallMounting.rotateOnAxis(new THREE.Vector3(0, 0, 1), rotation);
      handrailWallMounting.position.copy(mountingCoordinates);
      this.scene.add(handrailWallMounting);
      this.handrailsWallMountings.push(handrailWallMounting);
    }
  },
}