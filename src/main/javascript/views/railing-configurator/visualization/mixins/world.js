import * as THREE from 'three';
import { disposeObjects } from '@/utils';

export default {
  data() {
    return {
      ground: null,
      sky: null,
      worldParams: {
        size: 100,
        groundHeight: 0.001
      }
    }
  },
  beforeDestroy() {
    this.removeWorld();
  },

  methods: {
    createWorld() {
      let geometry = new THREE.BoxBufferGeometry(this.worldParams.size, this.worldParams.groundHeight, this.worldParams.size);
      this.groundPlane = new THREE.Mesh(geometry, new THREE.ShadowMaterial({ opacity: 0.7 }));
      this.groundPlane.position.setY(-this.platformParams.height);
      this.groundPlane.receiveShadow = true;
      this.scene.add(this.groundPlane);
    },

    removeWorld() {
      this.scene.remove(this.groundPlane);
      disposeObjects([this.groundPlane])
      this.groundPlane = null;
    },
  },
}