import * as THREE from 'three';
import { SetupItemName, SetupItemTypeName } from "@/static";
import { disposeObjects } from '@/utils';

export default {
  data() {
    return {
      columns: [],
      columnParams: {
        profile: this.setup[SetupItemName.PROFILE].optionId,
        height: this.setup[SetupItemName.HEIGHT].optionId,
        radius: 0.0205
      },
    }
  },
  watch: {
    [`setup.${SetupItemName.HEIGHT}.optionId`](height) {
      this.columnParams.height = height;
      this.redrawColumns();
    },
    [`setup.${SetupItemName.PROFILE}.optionId`](profile) {
      this.columnParams.profile = profile;
      this.redrawColumns();
    },
    [`setup.${SetupItemName.COLOR}.optionId`]() {
      this.redrawColumns();
    },
    [`setup.${SetupItemName.MOUNTING}.optionId`]() {
      this.redrawColumns();
    },
  },
  beforeDestroy() {
    this.removeColumns();
  },
  methods: {
    redrawColumns() {
      this.removeColumns();
      this.createColumns();
      this.renderer.render(this.scene, this.camera);
    },

    createColumns() {
      this.mountingsCoordinates.forEach(coordinates => {
        const column = this.createColumn();
        this.positionColumnInScene(column, coordinates);
      });
    },

    createColumn() {
      const { radius, profile } = this.columnParams;
      let columnGeometry;
      if (profile !== SetupItemTypeName.PROFILE_SQUARE) {
        columnGeometry = new THREE.CylinderBufferGeometry(radius, radius, this.getHeight(), this.commonParams.radialSegments);
      } else {
        columnGeometry = new THREE.BoxBufferGeometry(radius * 2, this.getHeight(), radius * 2);
      }
      let column = new THREE.Mesh(columnGeometry, this.getColor());
      column.castShadow = true;

      return column;
    },

    removeColumns() {
      this.scene.remove(...this.columns);
      disposeObjects(this.columns);
      this.columns = [];
    },

    positionColumnInScene(column, columnParams) {
      if (columnParams.wallMount) {
        disposeObjects([column]);
        return;
      }
      column.position.copy(columnParams);
      this.scene.add(column);
      this.columns.push(column);
    }
  },
}