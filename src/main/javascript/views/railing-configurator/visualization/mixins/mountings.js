import * as THREE from 'three';
import {SetupItemName, SetupItemTypeName} from "@/static";
import {disposeObjects} from "@/utils";

export default {
  data() {
    return {
      mountings: [],
      mountingParams: {
        type: this.setup[SetupItemName.MOUNTING].optionId,
        profile: this.setup[SetupItemName.PROFILE].optionId,
        wallPartHeight: 0.10,
        wallPartWidth: 0.04,
        wallPartDepth: 0.01,
        supportDistance: 0.030, //distance from the center of the wall mounting
        supportRadius: 0.007,
        supportOuterPartLength: 0.01,
        groundMountingRadius: 0.045,
        groundMountingHeight: 0.015,
      },
    }
  },
  watch: {
    [`setup.${SetupItemName.PROFILE}.optionId`](profile) {
      this.mountingParams.profile = profile;
      this.redrawMountings();
    },
    [`setup.${SetupItemName.COLOR}.optionId`]() {
      this.redrawMountings();
    },
    [`setup.${SetupItemName.MOUNTING}.optionId`](mounting) {
      this.mountingParams.type = mounting;
      this.redrawMountings();
    }
  },
  methods: {
    redrawMountings() {
      this.removeMountings();
      this.createMountings();
      this.renderer.render(this.scene, this.camera);
    },

    beforeDestroy() {
      this.removeMountings();
    },

    createMountings() {
      const mountingSideHeight = this.getMountingSideHeight();
      let mountingsParams = this.calculateMountingParams(this.mountingsCoordinates, mountingSideHeight);
      mountingsParams.forEach(mountingParams => {
        let mounting;
        if (mountingParams.mountingSideHeight > 0) {
          mounting = this.createSideMounting(mountingParams);
        } else {
          mounting = this.createGroundMounting(mountingParams);
        }
        this.positionMountingInScene(mounting, mountingParams);
      });
    },

    getMountingSideHeight() {
      if (this.mountingParams.type && this.mountingParams.type.startsWith('MOUNTING_SIDE')) {
        return parseInt(this.mountingParams.type.split('_')[2]) / 1000;
      } else {
        return 0;
      }
    },

    calculateMountingParams(mountingsCoordinates, mountingSideHeight) {
      let mountingsCount = this.mountingsCoordinates.length;
      return mountingsCoordinates.reduce((acc, currCorner, i) => {

        const isFirstCorner = i === 0;
        const isLastCorner = i === mountingsCount - 1;
        let prevCorner = isFirstCorner ? this.mountingsCoordinates[mountingsCount - 1] : this.mountingsCoordinates[i - 1];
        let nextCorner = isLastCorner ? this.mountingsCoordinates[0] : this.mountingsCoordinates[i + 1];

        let rotationPrev = isFirstCorner ? this.calculateRotationBetweenTwoPoints(currCorner, nextCorner) : this.calculateRotationBetweenTwoPoints(prevCorner, currCorner);
        let rotationNext = isLastCorner ? this.calculateRotationBetweenTwoPoints(prevCorner, currCorner) : this.calculateRotationBetweenTwoPoints(currCorner, nextCorner);

        if (currCorner.wallMount) {
          return acc;
        } else {
          return [...acc, {
            position: currCorner,
            rotationPrev,
            wallDirection: currCorner.wallDirection,
            rotationNext,
            mountingSideHeight
          }];
        }
      }, []);
    },

    createGroundMounting() {
      let groundSupport;
      if (this.mountingParams.profile !== SetupItemTypeName.PROFILE_SQUARE) {
        groundSupport = new THREE.CylinderBufferGeometry(this.mountingParams.groundMountingRadius,
          this.mountingParams.groundMountingRadius,
          this.mountingParams.groundMountingHeight,
          this.commonParams.radialSegments);
      } else {
        groundSupport = new THREE.BoxBufferGeometry(this.mountingParams.groundMountingRadius * 2,
          this.mountingParams.groundMountingHeight,
          this.mountingParams.groundMountingRadius * 2);
      }
      groundSupport.translate(0, -this.getHeight() / 2 + this.mountingParams.groundMountingHeight / 2, 0);

      let groundSupportMesh = new THREE.Mesh(groundSupport, this.getColor());
      groundSupportMesh.castShadow = true;

      return groundSupportMesh;
    },

    createSideMounting(mountingParams) {
      const { radius, profile } = this.columnParams;
      const additionalColumnLength = mountingParams.mountingSideHeight;
      const columnShift = -this.getHeight() / 2 - additionalColumnLength / 2;
      let columnGeometry;
      if (profile !== SetupItemTypeName.PROFILE_SQUARE) {
        columnGeometry = new THREE.CylinderBufferGeometry(radius, radius, additionalColumnLength);
      } else {
        columnGeometry = new THREE.BoxBufferGeometry(radius * 2, additionalColumnLength, radius * 2);
      }
      columnGeometry.translate(0, columnShift, 0);
      let columnPart = new THREE.Mesh(columnGeometry, this.getColor());
      columnPart.castShadow = true;

      let partOnWallShift = columnShift - additionalColumnLength / 2 + this.mountingParams.wallPartHeight / 2;
      let partOnWallPrev = new THREE.BoxBufferGeometry(this.mountingParams.wallPartWidth,
        this.mountingParams.wallPartHeight,
        this.mountingParams.wallPartDepth);
      partOnWallPrev.translate(-this.mountingParams.wallPartWidth / 2,
        partOnWallShift,
        0);
      partOnWallPrev.rotateY(mountingParams.rotationPrev);
      partOnWallPrev.translate(mountingParams.wallDirection.x,
        mountingParams.wallDirection.y,
        mountingParams.wallDirection.z);
      let partOnWallGeometryPrev = new THREE.Mesh(partOnWallPrev, this.getColor());

      let partOnWallNext = new THREE.BoxBufferGeometry(this.mountingParams.wallPartWidth,
        this.mountingParams.wallPartHeight,
        this.mountingParams.wallPartDepth);
      partOnWallNext.translate(this.mountingParams.wallPartWidth / 2,
        partOnWallShift,
        0);
      partOnWallNext.rotateY(mountingParams.rotationNext);
      partOnWallNext.translate(mountingParams.wallDirection.x,
        mountingParams.wallDirection.y,
        mountingParams.wallDirection.z);
      let partOnWallGeometryNext = new THREE.Mesh(partOnWallNext, this.getColor());

      let lowerSupport = new THREE.CylinderBufferGeometry(this.mountingParams.supportRadius,
        this.mountingParams.supportRadius,
        mountingParams.wallDirection.length() + this.columnParams.radius * 2 + this.mountingParams.supportOuterPartLength * 2);
      lowerSupport.rotateX(Math.PI / 2);
      lowerSupport.rotateY(Math.PI / 2 + this.calculateRotationBetweenTwoPoints(new THREE.Vector3(), mountingParams.wallDirection));
      lowerSupport.translate(mountingParams.wallDirection.x / 2,
        partOnWallShift + this.mountingParams.supportDistance,
        mountingParams.wallDirection.z / 2);
      let lowerSupportMesh = new THREE.Mesh(lowerSupport, this.getColor());
      lowerSupportMesh.castShadow = true;

      let upperSupport = lowerSupport.clone();
      upperSupport.translate(0, -2 * this.mountingParams.supportDistance, 0);
      let upperSupportMesh = new THREE.Mesh(upperSupport, this.getColor());
      upperSupportMesh.castShadow = true;

      let mountingGroup = new THREE.Group();
      mountingGroup.add(columnPart);
      mountingGroup.add(partOnWallGeometryPrev);
      mountingGroup.add(partOnWallGeometryNext);
      mountingGroup.add(lowerSupportMesh);
      mountingGroup.add(upperSupportMesh);

      return mountingGroup;
    },

    removeMountings() {
      this.scene.remove(...this.mountings);
      disposeObjects(this.mountings);
      this.mountings = [];
    },

    positionMountingInScene(mounting, mountingParams) {
      mounting.position.copy(mountingParams.position);
      this.scene.add(mounting);
      this.mountings.push(mounting);
    }
  },
}