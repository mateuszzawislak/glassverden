import throttle from 'lodash.throttle';

const ZOOM_EVENT_THROTTLE_TIME = 50; //[ms]

export default {
  data() {
    return {
      performRefresh: false,
    }
  },
  mounted() {
    this.$refs.canvas.addEventListener('wheel', this.zoomThrottle = throttle(this.updatePointOfView, ZOOM_EVENT_THROTTLE_TIME), true);
    this.$refs.canvas.addEventListener('mousedown', this.startRefresh);
    this.$refs.canvas.addEventListener('touchstart', this.startRefresh);
  },
  beforeDestroy() {
    this.$refs.canvas.removeEventListener('wheel', this.zoomThrottle);
    this.$refs.canvas.removeEventListener('mousedown', this.startRefresh);
    this.$refs.canvas.removeEventListener('touchstart', this.startRefresh);
  },
  methods: {
    refreshPointOfView() {
      if (this.performRefresh) {
        requestAnimationFrame(this.refreshPointOfView);
      }

      this.updatePointOfView();
    },
    updatePointOfView() {
      this.renderer.render(this.scene, this.camera);
    },
    startRefresh() {
      this.$refs.canvas.addEventListener('mouseup', this.stopRefresh);
      this.$refs.canvas.addEventListener('mouseleave', this.stopRefresh);
      this.$refs.canvas.addEventListener('touchend', this.stopRefresh);
      this.performRefresh = true;
      this.refreshPointOfView();
    },
    stopRefresh() {
      this.performRefresh = false;
      this.$refs.canvas.removeEventListener('mouseup', this.stopRefresh);
      this.$refs.canvas.removeEventListener('mouseleave', this.stopRefresh);
      this.$refs.canvas.removeEventListener('touchend', this.stopRefresh);
    },
  },
}