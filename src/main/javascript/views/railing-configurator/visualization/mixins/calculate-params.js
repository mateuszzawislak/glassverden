import * as THREE from 'three';
import { SetupItemName, SetupItemTypeName } from '@/static';

export default {
  data() {
    return {
      cornersCoordinates: null,
      maxEdgeLength: null,
      commonParams: {
        height: this.setup[SetupItemName.HEIGHT].optionId,
        color: this.setup[SetupItemName.COLOR].optionId,
        mounting: this.setup[SetupItemName.MOUNTING].optionId,
        handrail: this.setup[SetupItemName.HANDRAIL].optionId,
        mountingStart: this.setup[SetupItemName.MOUNTING_START].optionId,
        mountingEnd: this.setup[SetupItemName.MOUNTING_END].optionId,
        lastElementShiftParam: 0.1, // distance from last column to side edge of platform
        mountingShiftParam: 0.09, //distance from center of column to platform edge
        radialSegments: 13 //accuracy of displaying round elements, the larger value the more accurate
      }
    }
  },
  watch: {
    [`setup.${SetupItemName.HEIGHT}.optionId`](height) {
      this.commonParams.height = height;
      this.calculateParams();
    },
    [`setup.${SetupItemName.COLOR}.optionId`](color) {
      this.commonParams.color = color;
    },
    [`setup.${SetupItemName.MOUNTING}.optionId`](mounting) {
      this.commonParams.mounting = mounting;
      this.calculateParams();
    },
    [`setup.${SetupItemName.HANDRAIL}.optionId`](handrail) {
      this.commonParams.handrail = handrail;
      this.calculateParams();
    },
    [`setup.${SetupItemName.MOUNTING_START}.optionId`](mountingStart) {
      this.commonParams.mountingStart = mountingStart;
      this.redrawHandrails();
    },
    [`setup.${SetupItemName.MOUNTING_END}.optionId`](mountingEnd) {
      this.commonParams.mountingEnd = mountingEnd;
      this.redrawHandrails();
    }
  },

  methods: {
    calculateParams() {
      this.calculateCornersCoordinates();
      this.calculateMountingsCoordinates();
      this.$emit('set-loader', false);
    },

    calculateCornersCoordinates() {
      const coordinates = [];
      let rotationAxis = new THREE.Vector3(0, 1, 0);

      let previousAngle = 0;
      let previousPoint = new THREE.Vector3(0, 0, 0);
      coordinates.push(previousPoint);

      for (const edge of this.setup[SetupItemName.EDGES].optionId) {
        let currentPoint = new THREE.Vector3(0, 0, edge.length / 1000);
        let currentAngle = previousAngle + THREE.Math.degToRad(edge.angle ? 180 - edge.angle : 0);
        currentPoint.applyAxisAngle(rotationAxis, currentAngle);

        currentPoint.add(previousPoint);

        previousPoint = currentPoint;
        previousAngle = currentAngle;
        coordinates.push(currentPoint);
      }

      let balusterSize = coordinates.reduce((acc, curr, i) => {
        acc.minX = Math.min(acc.minX, curr.x);
        acc.maxX = Math.max(acc.maxX, curr.x);
        acc.minZ = Math.min(acc.minZ, curr.z);
        acc.maxZ = Math.max(acc.maxZ, curr.z);
        return acc;
      }, { minX: 0, maxX: 0, minZ: 0, maxZ: 0 });
      let balusterXLength = balusterSize.maxX - balusterSize.minX;
      let balusterZLength = balusterSize.maxZ - balusterSize.minZ;
      this.maxEdgeLength = Math.max(balusterZLength, balusterXLength);

      let deltaX = balusterXLength / 2 - balusterSize.maxX;
      let deltaZ = balusterZLength / 2 - balusterSize.maxZ;

      //move all points so that it is centered
      let positionVector = new THREE.Vector3(deltaX,
        this.getHeight() / 2, deltaZ);

      for (const coordinate of coordinates) {
        coordinate.add(positionVector);
      }

      this.cornersCoordinates = coordinates;
    },

    calculateMountingsCoordinates() {
      let cornersCount = this.cornersCoordinates.length;
      let mountingsOnCorners =
        this.cornersCoordinates.reduce((acc, currCorner, i) => {

          const isFirstCorner = i === 0;
          const isLastCorner = i === cornersCount - 1;
          let prevCorner = isFirstCorner ? this.cornersCoordinates[cornersCount - 1] : this.cornersCoordinates[i - 1];
          let nextCorner = isLastCorner ? this.cornersCoordinates[0] : this.cornersCoordinates[i + 1];

          let rotationToPrev = this.calculateRotationBetweenTwoPoints(currCorner, prevCorner);
          let rotationToNext = this.calculateRotationBetweenTwoPoints(nextCorner, currCorner);

          let tangentToPrev;
          let tangentToNext;
          if (this.isProjectWithSingleEdge()) {
            tangentToPrev = 0;
            tangentToNext = 0;
          } else {
            tangentToPrev = rotationToPrev + Math.PI / 2;
            tangentToNext = rotationToNext + Math.PI / 2;
          }

          let shiftVector;
          if (this.commonParams.mounting === SetupItemTypeName.MOUNTING_GROUND) {
            shiftVector = new THREE.Vector3(-this.commonParams.mountingShiftParam, 0, 0);
          } else {
            shiftVector = new THREE.Vector3(this.commonParams.mountingShiftParam, 0, 0);
          }
          let shiftVectorAxis = new THREE.Vector3(0, 1, 0);
          let shiftVectorOnPrev = shiftVector.clone().applyAxisAngle(shiftVectorAxis, tangentToPrev);
          let shiftVectorOnNext = shiftVector.clone().applyAxisAngle(shiftVectorAxis, tangentToNext);

          let prevPointShiftedByPrev = prevCorner.clone().add(shiftVectorOnPrev);
          let currPointShiftedByPrev = currCorner.clone().add(shiftVectorOnPrev);
          let nextPointShiftedByNext = nextCorner.clone().add(shiftVectorOnNext);
          let currPointShiftedByNext = currCorner.clone().add(shiftVectorOnNext);

          let lineIntersection = this.checkLineIntersection(
            prevPointShiftedByPrev.x, prevPointShiftedByPrev.z,
            currPointShiftedByPrev.x, currPointShiftedByPrev.z,
            nextPointShiftedByNext.x, nextPointShiftedByNext.z,
            currPointShiftedByNext.x, currPointShiftedByNext.z);

          if (isFirstCorner) {
            currPointShiftedByNext.shiftVectorTowardWall = shiftVectorOnNext;
            if (this.commonParams.mountingStart !== SetupItemTypeName.MOUNTING_START_HANDRAIL_AND_GLASS) {
              currPointShiftedByNext.add(this.calculateShiftVectorBetweenTwoPoints(nextPointShiftedByNext, currPointShiftedByNext, this.commonParams.lastElementShiftParam));
            } else {
              currPointShiftedByNext.wallMount = true;
            }
            acc.push(currPointShiftedByNext);
          } else if (isLastCorner) {
            currPointShiftedByPrev.shiftVectorTowardWall = shiftVectorOnPrev;
            if (this.commonParams.mountingEnd !== SetupItemTypeName.MOUNTING_END_HANDRAIL_AND_GLASS) {
              currPointShiftedByPrev.add(this.calculateShiftVectorBetweenTwoPoints(prevPointShiftedByPrev, currPointShiftedByPrev, this.commonParams.lastElementShiftParam));
            } else {
              currPointShiftedByPrev.wallMount = true;
            }
            acc.push(currPointShiftedByPrev);
          } else {
            let newMountingOnCorner = new THREE.Vector3(lineIntersection.x, currCorner.y, lineIntersection.y);
            newMountingOnCorner.shiftVectorTowardWall = shiftVectorOnNext;
            newMountingOnCorner.relatedCorner = currCorner;
            acc.push(newMountingOnCorner);
          }
          return acc;
        }, []);

      this.mountingsCoordinates =
        mountingsOnCorners.reduce((acc, currMounting, i) => {

          let nextMounting = i === mountingsOnCorners.length - 1 ? mountingsOnCorners[0] : mountingsOnCorners[i + 1];

          let segmentsNumber = 1;
          if (i < this.setup[SetupItemName.EDGES].optionId.length) {
            segmentsNumber = this.setup[SetupItemName.EDGES].optionId[i].segmentsNumber;
          }

          for (let segmentNumber = 0; segmentNumber < segmentsNumber; segmentNumber++) {
            let pointToAdd = new THREE.Vector3(currMounting.x + (nextMounting.x - currMounting.x) / segmentsNumber * segmentNumber,
              currMounting.y,
              currMounting.z + (nextMounting.z - currMounting.z) / segmentsNumber * segmentNumber);

            if (segmentNumber === 0 && currMounting.relatedCorner) {
              pointToAdd.wallDirection = this.calculateShiftVectorBetweenTwoPoints(currMounting.relatedCorner, pointToAdd);
            } else {
              pointToAdd.wallDirection = currMounting.shiftVectorTowardWall.clone().negate();
            }
            pointToAdd.wallMount = segmentNumber === 0 && currMounting.wallMount;
            acc.push(pointToAdd);
          }

          return acc;
        }, []);
    },

    calculateShiftVectorBetweenTwoPoints(startingPoint, endingPoint, shiftLength) {
      let shiftDirection = new THREE.Vector3();
      shiftDirection.subVectors(startingPoint, endingPoint);

      if (shiftLength !== undefined) {
        shiftDirection.normalize().multiplyScalar(shiftLength);
      }

      return shiftDirection;
    },

    calculateRotationBetweenTwoPoints(startingPoint, endingPoint) {
      let rotation = 0 - new THREE.Vector3(endingPoint.x - startingPoint.x, 0, endingPoint.z - startingPoint.z).normalize()
        .angleTo(new THREE.Vector3(1, 0, 0));
      //angleTo returns always positive value so sometimes we have to negate it
      if (endingPoint.z - startingPoint.z < 0) {
        rotation = 0 - rotation;
      }

      return rotation;
    },

    getHeight() {
      const selectedHeight = this.commonParams.height;
      let totalHeight = 0;

      if (selectedHeight === SetupItemTypeName.HEIGHT_900) {
        totalHeight = 0.9;
      } else if (selectedHeight === SetupItemTypeName.HEIGHT_1200) {
        totalHeight = 1.2;
      } else {
        totalHeight = 1;
      }

      let handrailHeightToCompensate = 0;
      if (this.commonParams.handrail !== SetupItemTypeName.HANDRAIL_NO_RAIL) {
        handrailHeightToCompensate = this.handrailParams.supportLength + 2 * this.handrailParams.radius;
      }

      return totalHeight - handrailHeightToCompensate;
    },

    getColor() {
      return new THREE.MeshStandardMaterial({
        color: '#fff',
        metalness: 1,
        roughness: this.commonParams.color === SetupItemTypeName.COLOR_SHINY ? 0.6 : 0.75,
      });
    },

    isProjectWithSingleEdge() {
      return this.setup[SetupItemName.EDGES].optionId.length === 1;
    },

    //http://jsfiddle.net/justin_c_rounds/Gd2S2/light/
    checkLineIntersection(line1StartX, line1StartY, line1EndX, line1EndY, line2StartX, line2StartY, line2EndX, line2EndY) {
      // if the lines intersect, the result contains the x and y of the intersection (treating the lines as infinite) and booleans for whether line segment 1 or line segment 2 contain the point
      var denominator, a, b, numerator1, numerator2, result = {
        x: (line1EndX + line2EndX) / 2,
        y: (line1EndY + line2EndY) / 2,
        onLine1: false,
        onLine2: false
      };
      denominator = ((line2EndY - line2StartY) * (line1EndX - line1StartX)) - ((line2EndX - line2StartX) * (line1EndY - line1StartY));
      if (denominator == 0) {
        return result;
      }
      a = line1StartY - line2StartY;
      b = line1StartX - line2StartX;
      numerator1 = ((line2EndX - line2StartX) * a) - ((line2EndY - line2StartY) * b);
      numerator2 = ((line1EndX - line1StartX) * a) - ((line1EndY - line1StartY) * b);
      a = numerator1 / denominator;
      b = numerator2 / denominator;

      // if we cast these lines infinitely in both directions, they intersect here:
      result.x = line1StartX + (a * (line1EndX - line1StartX));
      result.y = line1StartY + (a * (line1EndY - line1StartY));
      /*
              // it is worth noting that this should be the same as:
              x = line2StartX + (b * (line2EndX - line2StartX));
              y = line2StartX + (b * (line2EndY - line2StartY));
              */
      // if line1 is a segment and line2 is infinite, they intersect if:
      if (a > 0 && a < 1) {
        result.onLine1 = true;
      }
      // if line2 is a segment and line1 is infinite, they intersect if:
      if (b > 0 && b < 1) {
        result.onLine2 = true;
      }
      // if line1 and line2 are segments, they intersect if both of the above are true
      return result;
    }
  },
}