import { SetupItemName } from '@/static';

export default {
  watch: {
    [`setup.${SetupItemName.EDGES}.optionId`]() {
      this.redraw();
    },
  },
  methods: {
    redraw() {
      this.calculateParams();
      this.redrawColumns();
      this.redrawPlatform();
      this.redrawGlasses();
      this.redrawGrips();
      this.redrawHandrails();
      this.redrawMountings();
      this.renderer.render(this.scene, this.camera);
    },
  },
}