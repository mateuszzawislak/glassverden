export default (...elements) => {
  elements.forEach((objects) => {
    objects.forEach((object) => {
      if (object.children) {
        object.children.forEach(removeObject);
      }
      removeObject(object);
    });
  });
}

function removeObject(object) {
  if (object.geometry) {
    object.geometry.dispose();
  }

  if (object.material) {
    object.material.dispose();
  }
}