const cryptoApi = window.crypto || window.msCrypto;
export default () => {
  const array = new Uint32Array(1);
  return cryptoApi.getRandomValues(array)[0];
}