export default (errorGroup, error) => {
  return errorGroup[error.message] || 'unknown';
};