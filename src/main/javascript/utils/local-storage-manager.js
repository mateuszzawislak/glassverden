class LocalStorageManager {
  getItem(name) {
    let value;
    try {
      value = JSON.parse(window.localStorage.getItem(name));
    } catch (e) {
      console.warn('Railing setup is broken', e);
    }

    return value;
  }

  setItem(name, value) {
    this.getItem(name);
    let localValue;

    try {
      localValue = JSON.stringify(value);
      window.localStorage.setItem(name, localValue);
    } catch (e) {
      console.warn('Cannot save item ', name, e);
    }

    return !!localValue;
  }

  clear(name, value) {
    window.localStorage.removeItem(name);
    if (value) {
      this.setItem(name, value);
    }
  }
}

export const StorageKey = {
  PROJECT_CODE: 'projectCode',
};

export default new LocalStorageManager();