import disposeObjects from './dispose-objects';
import getRandomHex from './get-random-hex';
import getError from './get-error';
import localStorageManager, { StorageKey } from './local-storage-manager';

export { disposeObjects };
export { getRandomHex };
export { getError };
export { localStorageManager };
export { StorageKey };