import Vue from 'vue';
export default new Vue();

export const EventType = {
  RESIZE: 'resize',
  RESIZE_BREAKPOINT: 'resizeBreakpoint',
  VISIBILITY_CHANGE: 'visibilityChange',
};