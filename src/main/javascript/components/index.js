import ConfigurableItem from './configurator-components/configurable-item.vue';
import FooterSection from './footer/index.vue';
import Modal from './modal/index.vue';
import PhotoGallery from './photo-gallery/index.vue';
import TopBar from './top-bar/index.vue';

export { ConfigurableItem };
export { FooterSection };
export { Modal };
export { PhotoGallery };
export { TopBar };