import { NewOptionValue, DimensionProp, SetupItemName, SetupItemTypeName, RAILING_TYPE_ID, DEFAULT_RAILING_TYPE } from '@/static';
import { localStorageManager, StorageKey } from '@/utils';
import { ProjectAdapter, ParametersAdapter, railingTypeConfig, getProperMountingSetup } from '@/libs/sdk';

const DEFAULT_EDITABLE_OPTION = {
  [DimensionProp.ANGLE]: null,
  [DimensionProp.SEGMENTS_NUMBER]: NewOptionValue[DimensionProp.SEGMENTS_NUMBER],
  [DimensionProp.LENGTH]: NewOptionValue[DimensionProp.LENGTH],
};
const NEW_EDITABLE_OPTION = { ...DEFAULT_EDITABLE_OPTION, angle: NewOptionValue[DimensionProp.ANGLE] };

const fillWithSelectedOption = (setup, setupItem) => {
  const saveditem = setup[setupItem.id];
  if (saveditem) {
    if ((setupItem.id === SetupItemName.MOUNTING_END
      || setupItem.id === SetupItemName.MOUNTING_START)
      && setup[SetupItemName.HANDRAIL].optionId === SetupItemTypeName.HANDRAIL_NO_RAIL) {
      setupItem.options = getProperMountingSetup(setupItem).options.map((item) => ({ ...item, selected: item.id === saveditem.optionId }));
    } else {
      setupItem.options = setupItem.options.map((item) => ({ ...item, selected: item.id === saveditem.optionId }));
    }
  }

  return { ...setupItem };
}

const fillEditableOption = (setup, setupItem) => {
  const { optionId } = setup[setupItem.id] || {};
  setupItem.options = optionId;
  return { ...setupItem };
}

const extractFetchedSetup = (setup, setupList) => {
  const setupSelected = setup.parameterOptions.map(item => item.code);
  return setupList.reduce((acc, setupItem) => {
    if (setupItem.id === SetupItemName.EDGES) {
      acc[SetupItemName.EDGES] = {
        editable: true,
        optionId: setup.edges,
      };

      return acc;
    }

    const foundItem = setupItem.options.find(item => setupSelected.includes(item.id));
    //TODO check missing item
    if (foundItem) {
      acc[setupItem.id] = {
        optionId: foundItem.id,
      }
    }

    return acc;
  }, {
    [RAILING_TYPE_ID]: {
      optionId: setup.type,
    }
  });
};

export default class RailingResource {
  constructor() {
    this.parameters = new ParametersAdapter();
    this.project = new ProjectAdapter();
    this.isFetched = false;
    this.store = {
      setup: {},
      setupList: [],
    }
  }

  fillSetup(extractedSetup, setupList) {
    return setupList.reduce((acc, item) => {
      const { id, editable } = item;
      const setupItem = extractedSetup[id];
      let optionId;

      if (editable) {
        optionId = setupItem && setupItem.optionId || [DEFAULT_EDITABLE_OPTION];
      } else {
        optionId = setupItem && setupItem.optionId;
      }

      let selectedOption = {
        optionId,
        editable,
      };

      return { ...acc, [id]: selectedOption };
    }, {})
  }

  fetchAll(projectType, forceNewProject) {
    return Promise.all([this.fetchProjectSetup(projectType, forceNewProject), this.parameters.get()])
      .then(([setup, setupList]) => {
        this.store.railingTypeConfig = railingTypeConfig;
        this.successMiddleware(setup, setupList, railingTypeConfig);
        this.isFetched = true;
      });
  }

  successMiddleware(setup, setupList = this.store.setupList, railingTypeConfig = this.store.railingTypeConfig) {
    const extractedSetup = extractFetchedSetup(setup, setupList);
    this.store.setup = this.fillSetup(extractedSetup, [railingTypeConfig, ...setupList]);
    this.store.setupList = this.updateSetupList(setupList);
  }

  fetchProjectSetup(projectType, forceNewProject) {
    const code = localStorageManager.getItem(StorageKey.PROJECT_CODE);
    if (!forceNewProject && code && (!projectType || projectType === this.project.projectType)) {
      return this.project.get(code);
    } else {
      return this.project.create(projectType || DEFAULT_RAILING_TYPE);
    }
  }

  addOption(id) {
    const setupItem = this.store.setup[id];
    const { optionId } = setupItem;

    if (Array.isArray(optionId)) {
      const options = [...optionId, { ...NEW_EDITABLE_OPTION }];
      const updatedSetup = { ...this.store.setup, [id]: { ...setupItem, optionId: options } };
      this.project.update(updatedSetup)
        .then(setup => this.successMiddleware(setup));
    }
  }

  setOption(id, optionId) {
    const setupItem = this.store.setup[id];
    let updatedSetup = { ...this.store.setup, [id]: { ...setupItem, optionId } };

    if (id === SetupItemName.HANDRAIL) {
      const { setupList, setup } = this.parameters.handrailUpdateProxy(updatedSetup, this.store.setupList, optionId);
      updatedSetup = setup;
      this.store.setupList = setupList;
    }

    if (id !== RAILING_TYPE_ID) {
      return this.project.update(updatedSetup)
        .then(setup => this.successMiddleware(setup));
    }

    return Promise.resolve();
  }

  removeOption(id, item) {
    const setupItem = this.store.setup[id];
    const { optionId } = setupItem;

    if (Array.isArray(optionId)) {
      const options = optionId.filter(optionItem => optionItem.id !== item.id);
      const updatedSetup = { ...this.store.setup, [id]: { ...setupItem, optionId: options } };
      this.project.update(updatedSetup)
        .then(setup => this.successMiddleware(setup));
    }
  }

  updateSetupList(setupList) {
    return setupList.reduce((acc, setupItem) => {
      let item;
      if (setupItem.editable) {
        item = fillEditableOption(this.store.setup, setupItem);
      } else {
        item = fillWithSelectedOption(this.store.setup, setupItem);
      }

      return [...acc, item];
    }, []);
  }
}