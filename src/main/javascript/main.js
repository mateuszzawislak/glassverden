import Vue from 'vue';
import App from '@/App.vue';
import router from '@/router';
import { languageManager } from '@/libs';
import { Services } from '@/libs/sdk';

Vue.config.productionTip = false;
Vue.prototype.$services = new Services();

new Vue({
  i18n: languageManager.i18n,
  router,
  render: h => h(App),
}).$mount('#app');
