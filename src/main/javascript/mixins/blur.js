import debounce from 'lodash.debounce';

export default {
  watch: {
    '$route': 'blurHandler',
  },
  created() {
    window.addEventListener('click', this.blurHandler);
    window.addEventListener('scroll', this.scrollBlurHandler = debounce(this.blurHandler, 1000, {
      leading: true,
      trailing: false,
    }));
  },
  beforeDestroy() {
    window.removeEventListener('click', this.blurHandler);
    window.removeEventListener('scroll', this.scrollBlurHandler);
  },
  methods: {
    blurHandler(e) {
      if (typeof this.onBlur === 'function') {
        this.onBlur(e && e.target);
      }
    }
  }
}