import EventBus, { EventType } from '@/eventbus';

export default {
  created() {
    window.addEventListener('visibilitychange', this.onVisibilityChange);
  },
  beforeDestroy() {
    window.removeEventListener('visibilitychange', this.onVisibilityChange);
  },
  methods: {
    onVisibilityChange() {
      EventBus.$emit(EventType.VISIBILITY_CHANGE, document.visibilityState === 'visible');;
    }
  }
}