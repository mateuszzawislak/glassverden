import throttle from 'lodash.throttle';
import EventBus, { EventType } from '@/eventbus';
import { breakPointValues } from '@/static';

const RESIZE_THROTTLE = 200;

export default {
  data() {
    return {
      breakPoints: breakPointValues,
      lastWidth: window.innerWidth,
    };
  },
  created() {
    window.addEventListener('resize', this.resizeThrottle = throttle(() => {
      const { innerWidth } = window;
      EventBus.$emit(EventType.RESIZE, innerWidth);
      
      if (this.hasBreakpointBeenReached(innerWidth)) {
        EventBus.$emit(EventType.RESIZE_BREAKPOINT, innerWidth);
      }

      this.lastWidth = innerWidth;
    }, RESIZE_THROTTLE));
  },
  beforeDestroy() {
    window.removeEventListener('resize', this.resizeThrottle);
  },
  methods: {
    hasBreakpointBeenReached(innerWidth) {
      if (!Array.isArray(this.breakPoints)) {
        this.breakPoints = isNaN(this.breakPoints) ? [] : [this.breakPoints];
      }

      return this.breakPoints.some((breakpoint) => {
        return (this.lastWidth >= breakpoint && innerWidth < breakpoint)
          || (this.lastWidth < breakpoint && innerWidth >= breakpoint)
      });
    }
  }
}