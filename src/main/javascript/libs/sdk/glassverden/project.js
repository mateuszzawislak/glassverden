import httpAdapter from './api-adapter';
import { localStorageManager, StorageKey } from '@/utils';
import { SetupItemName, RAILING_TYPE_ID } from '@/static';

const NEW_PROJECT_STATUS_NAME = 'NEW';

export default class Project {
  constructor() {
    this.endpoint = '/projects';
    this.code = undefined;
    this.projectType = undefined;
    this.grossPrice = undefined;
    this.netPrice = undefined;
    this.status = undefined;
    this.contact = {};
  }

  get isCompleted() {
    return this.status !== NEW_PROJECT_STATUS_NAME;
  }

  create(projectType) {
    if (!projectType) {
      throw new Error('Parameter projectType is required!');
    }

    return httpAdapter.create(`${this.endpoint}?type=${projectType}`)
      .then(httpAdapter.deserialize)
      .then((data) => {
        const { code, grossPrice, netPrice, status } = data;
        this.code = code;
        this.projectType = projectType;
        this.grossPrice = grossPrice;
        this.netPrice = netPrice;
        this.status = status;
        this.contact = {};

        localStorageManager.setItem(StorageKey.PROJECT_CODE, code);
        //TODO remove if api ensures order
        data.edges.sort((a, b) => a.rank - b.rank);
        return data;
      });
  }

  get(code) {
    this.code = code;
    return httpAdapter.fetch(`${this.endpoint}/${code}`)
      .then(httpAdapter.deserialize)
      .then((data) => {
        const { type, grossPrice, netPrice, status, contact } = data;
        this.projectType = type;
        this.grossPrice = grossPrice;
        this.netPrice = netPrice;
        this.status = status;
        this.contact = contact;

        return data;
      });
  }

  update(data) {
    const preparedData = this.prepareSetupToUpdate(data);
    return httpAdapter.update(`${this.endpoint}/${this.code}`, { code: this.code, ...preparedData })
      .then(httpAdapter.deserialize)
      .then((data) => {
        const { grossPrice, netPrice } = data;
        this.grossPrice = grossPrice;
        this.netPrice = netPrice;

        return data;
      });
  }

  prepareSetupToUpdate(setup) {
    const parameterOptions = Object.keys(setup).map((key, i) => {
      if (key !== SetupItemName.EDGES && key !== RAILING_TYPE_ID && setup[key].optionId)
        return { code: setup[key].optionId, rank: i };
    })
      //TODO check empty object in array
      .filter(Boolean);
    const edges = setup[SetupItemName.EDGES].optionId.map((edge, i) => ({ ...edge, rank: i }));

    return { parameterOptions, edges, type: setup[RAILING_TYPE_ID].optionId };
  }
}