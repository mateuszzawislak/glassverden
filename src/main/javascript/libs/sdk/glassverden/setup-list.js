import { SetupItemName, SetupItemTypeName, SetupItemTypeLabel, RAILING_TYPE_ID } from '@/static';

export const setupList = {
  [SetupItemName.EDGES]: {
    name: 'edges',
    editable: true,
    options: [],
  },
  [SetupItemName.COLOR]: {
    name: 'color',
    options: {
      [SetupItemTypeName.COLOR_SHINY]: {
        optionId: SetupItemTypeName.COLOR_SHINY,
        label: 'shiny',
        img: 'thumbnail-color-shiny',
      },
      [SetupItemTypeName.COLOR_MATT]: {
        optionId: SetupItemTypeName.COLOR_MATT,
        label: 'matt',
        img: 'thumbnail-color-matt',
      },
    },
  },
  [SetupItemName.GLASS_COLOR]: {
    name: 'glassColor',
    options: {
      [SetupItemTypeName.GLASS_COLOR_TRANSPARENT]: {
        optionId: SetupItemTypeName.GLASS_COLOR_TRANSPARENT,
        label: 'transparent',
        img: 'thumbnail-glass-color-transparent',
      },
      [SetupItemTypeName.GLASS_COLOR_GREY]: {
        optionId: SetupItemTypeName.GLASS_COLOR_GREY,
        label: 'grey',
        img: 'thumbnail-glass-color-gray',
      },
      [SetupItemTypeName.GLASS_COLOR_BROWN]: {
        optionId: SetupItemTypeName.GLASS_COLOR_BROWN,
        label: 'brown',
        img: 'thumbnail-glass-color-brown',
      },
      [SetupItemTypeName.GLASS_COLOR_WHITE]: {
        optionId: SetupItemTypeName.GLASS_COLOR_WHITE,
        label: 'white',
        img: 'thumbnail-glass-color-white',
      },
    },
  },
  [SetupItemName.GLASS_THICKNESS]: {
    name: 'glassThickness',
    options: {
      [SetupItemTypeName.GLASS_THICKNESS_44_2]: {
        optionId: SetupItemTypeName.GLASS_THICKNESS_44_2,
        label: 'thickness44',
        img: 'logo-without-name',
      },
      [SetupItemTypeName.GLASS_THICKNESS_55_2]: {
        optionId: SetupItemTypeName.GLASS_THICKNESS_55_2,
        label: 'thickness55',
        img: 'logo-without-name',
      },
      [SetupItemTypeName.GLASS_THICKNESS_66_2]: {
        optionId: SetupItemTypeName.GLASS_THICKNESS_66_2,
        label: 'thickness66',
        img: 'logo-without-name',
      },
    },
  },
  [SetupItemName.GLASS_TYPE]: {
    name: 'glassType',
    options: {
      [SetupItemTypeName.GLASS_NORMAL]: {
        optionId: SetupItemTypeName.GLASS_NORMAL,
        label: SetupItemTypeLabel.GLASS_NORMAL,
        img: 'logo-without-name',
      },
      [SetupItemTypeName.GLASS_TEMPERED]: {
        optionId: SetupItemTypeName.GLASS_TEMPERED,
        label: SetupItemTypeLabel.GLASS_TEMPERED,
        img: 'logo-without-name',
      },
    },
  },
  [SetupItemName.HANDRAIL]: {
    name: 'handrail',
    options: {
      [SetupItemTypeName.HANDRAIL_NO_RAIL]: {
        optionId: SetupItemTypeName.HANDRAIL_NO_RAIL,
        label: 'handrailNoRail',
        img: 'thumbnail-no-handrail',
      },
      [SetupItemTypeName.HANDRAIL_WITH_RAIL]: {
        optionId: SetupItemTypeName.HANDRAIL_WITH_RAIL,
        label: 'handrailWithRail',
        img: 'thumbnail-handrail',
      },
    },
  },
  [SetupItemName.HEIGHT]: {
    name: 'height',
    options: {
      [SetupItemTypeName.HEIGHT_900]: {
        optionId: SetupItemTypeName.HEIGHT_900,
        label: SetupItemTypeLabel.HEIGHT_900,
        img: 'logo-without-name',
      },
      [SetupItemTypeName.HEIGHT_1000]: {
        optionId: SetupItemTypeName.HEIGHT_1000,
        label: SetupItemTypeLabel.HEIGHT_1000,
        img: 'logo-without-name',
      },
      [SetupItemTypeName.HEIGHT_1200]: {
        optionId: SetupItemTypeName.HEIGHT_1200,
        label: SetupItemTypeLabel.HEIGHT_1200,
        img: 'logo-without-name',
      },
    },
  },
  [SetupItemName.MOUNTING]: {
    name: 'mounting',
    options: {
      [SetupItemTypeName.MOUNTING_GROUND]: {
        optionId: SetupItemTypeName.MOUNTING_GROUND,
        label: SetupItemTypeLabel.MOUNTING_GROUND,
        img: 'thumbnail-mounting-ground',
      },
      [SetupItemTypeName.MOUNTING_SIDE_120]: {
        optionId: SetupItemTypeName.MOUNTING_SIDE_120,
        label: SetupItemTypeLabel.MOUNTING_SIDE_120,
        img: 'thumbnail-mounting-side',
      },
      [SetupItemTypeName.MOUNTING_SIDE_135]: {
        optionId: SetupItemTypeName.MOUNTING_SIDE_135,
        label: SetupItemTypeLabel.MOUNTING_SIDE_135,
        img: 'thumbnail-mounting-side',
      },
      [SetupItemTypeName.MOUNTING_SIDE_150]: {
        optionId: SetupItemTypeName.MOUNTING_SIDE_150,
        label: SetupItemTypeLabel.MOUNTING_SIDE_150,
        img: 'thumbnail-mounting-side',
      },
      [SetupItemTypeName.MOUNTING_SIDE_165]: {
        optionId: SetupItemTypeName.MOUNTING_SIDE_165,
        label: SetupItemTypeLabel.MOUNTING_SIDE_165,
        img: 'thumbnail-mounting-side',
      },
      [SetupItemTypeName.MOUNTING_SIDE_180]: {
        optionId: SetupItemTypeName.MOUNTING_SIDE_180,
        label: SetupItemTypeLabel.MOUNTING_SIDE_180,
        img: 'thumbnail-mounting-side',
      },
      [SetupItemTypeName.MOUNTING_SIDE_195]: {
        optionId: SetupItemTypeName.MOUNTING_SIDE_195,
        label: SetupItemTypeLabel.MOUNTING_SIDE_195,
        img: 'thumbnail-mounting-side',
      },
      [SetupItemTypeName.MOUNTING_SIDE_210]: {
        optionId: SetupItemTypeName.MOUNTING_SIDE_210,
        label: SetupItemTypeLabel.MOUNTING_SIDE_210,
        img: 'thumbnail-mounting-side',
      },
      [SetupItemTypeName.MOUNTING_SIDE_225]: {
        optionId: SetupItemTypeName.MOUNTING_SIDE_225,
        label: SetupItemTypeLabel.MOUNTING_SIDE_225,
        img: 'thumbnail-mounting-side',
      },
      [SetupItemTypeName.MOUNTING_SIDE_240]: {
        optionId: SetupItemTypeName.MOUNTING_SIDE_240,
        label: SetupItemTypeLabel.MOUNTING_SIDE_240,
        img: 'thumbnail-mounting-side',
      },
      [SetupItemTypeName.MOUNTING_SIDE_255]: {
        optionId: SetupItemTypeName.MOUNTING_SIDE_255,
        label: SetupItemTypeLabel.MOUNTING_SIDE_255,
        img: 'thumbnail-mounting-side',
      },
    },
  },
  [SetupItemName.MOUNTING_END]: {
    name: 'mountingEnd',
    options: {
      [SetupItemTypeName.MOUNTING_END_NOTHING]: {
        optionId: SetupItemTypeName.MOUNTING_END_NOTHING,
        label: 'nothing',
        img: 'thumbnail-mounting-nothing',
      },
      [SetupItemTypeName.MOUNTING_END_HANDRAIL]: {
        optionId: SetupItemTypeName.MOUNTING_END_HANDRAIL,
        label: 'handrail',
        img: 'thumbnail-mounting-handrail',
      },
      [SetupItemTypeName.MOUNTING_END_HANDRAIL_AND_GLASS]: {
        optionId: SetupItemTypeName.MOUNTING_END_HANDRAIL_AND_GLASS,
        label: 'handrailAndGlass',
        labelAlt: 'glass',
        img: 'thumbnail-mounting-handrail-and-glass',
      },
    },
  },
  [SetupItemName.MOUNTING_START]: {
    name: 'mountingStart',
    options: {
      [SetupItemTypeName.MOUNTING_START_NOTHING]: {
        optionId: SetupItemTypeName.MOUNTING_START_NOTHING,
        label: 'nothing',
        img: 'thumbnail-mounting-nothing',
      },
      [SetupItemTypeName.MOUNTING_START_HANDRAIL]: {
        optionId: SetupItemTypeName.MOUNTING_START_HANDRAIL,
        label: 'handrail',
        img: 'thumbnail-mounting-handrail',
      },
      [SetupItemTypeName.MOUNTING_START_HANDRAIL_AND_GLASS]: {
        optionId: SetupItemTypeName.MOUNTING_START_HANDRAIL_AND_GLASS,
        label: 'handrailAndGlass',
        labelAlt: 'glass',
        img: 'thumbnail-mounting-handrail-and-glass',
      },
    },
  },
  [SetupItemName.PROFILE]: {
    name: 'profile',
    options: {
      [SetupItemTypeName.PROFILE_ROUND]: {
        optionId: SetupItemTypeName.PROFILE_ROUND,
        label: 'round',
        img: 'thumbnail-profile-round',
      },
      [SetupItemTypeName.PROFILE_SQUARE]: {
        optionId: SetupItemTypeName.PROFILE_SQUARE,
        label: 'square',
        img: 'thumbnail-profile-square',
      },
    },
  },
  [SetupItemName.STEEL_CLASS]: {
    name: 'steelClass',
    options: {
      [SetupItemTypeName.STEEL_CLASS_AISI316]: {
        optionId: SetupItemTypeName.STEEL_CLASS_AISI316,
        label: 'AISI316',
        img: 'logo-without-name',
      },
      [SetupItemTypeName.STEEL_CLASS_AISI304]: {
        optionId: SetupItemTypeName.STEEL_CLASS_AISI304,
        label: 'AISI304',
        img: 'logo-without-name',
      },
    },
  },
  [RAILING_TYPE_ID]: {
    id: RAILING_TYPE_ID,
    name: 'railingType',
    options: {
      [SetupItemTypeName.RAILING_WITH_BAR]: {
        id: SetupItemTypeName.RAILING_WITH_BAR,
        optionId: SetupItemTypeName.RAILING_WITH_BAR,
        name: 'railingTypeBar',
      },
      [SetupItemTypeName.RAILING_WITH_POSTS]: {
        id: SetupItemTypeName.RAILING_WITH_POSTS,
        optionId: SetupItemTypeName.RAILING_WITH_POSTS,
        name: 'railingTypePosts',
      },
      [SetupItemTypeName.RAILING_WITH_ROTULES]: {
        id: SetupItemTypeName.RAILING_WITH_ROTULES,
        optionId: SetupItemTypeName.RAILING_WITH_ROTULES,
        name: 'railingTypeRotules',
      },
    },
  },
};

export const railingTypeConfig = {
  id: RAILING_TYPE_ID,
  name: 'railingType',
  options: [
    {
      id: SetupItemTypeName.RAILING_WITH_BAR,
      name: 'railingTypeBar',
    },
    {
      id: SetupItemTypeName.RAILING_WITH_POSTS,
      name: 'railingTypePosts',
    },
    {
      id: SetupItemTypeName.RAILING_WITH_ROTULES,
      name: 'railingTypeRotules',
    },
  ]
};