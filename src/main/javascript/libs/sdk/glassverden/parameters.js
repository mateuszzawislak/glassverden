import httpAdapter from './api-adapter';
import { setupList } from './setup-list';
import { SetupItemName, SetupItemTypeName } from '@/static';

const convertConfig = (config, pattern = setupList) => {
  return config.reduce((acc, item) => {
    const foundItem = { ...pattern[item.code] };
    if (foundItem) {
      if (item.parameterOptions) {
        foundItem.options = convertConfig(item.parameterOptions, foundItem.options);
      } else {
        foundItem.optionId = item.code;
      }

      acc.push({ ...foundItem, id: item.code });
    }

    return acc;
  }, []);
};

const getMountingOptions = (currentOptions, idToRemove, idToReplace) => {
  return currentOptions.filter(option => option.id !== idToRemove)
    .map(option => ({ ...option, label: option.id === idToReplace ? option.labelAlt : option.label }));
};

export const getProperMountingSetup = (setupItem) => {
  switch (setupItem.id) {
    case SetupItemName.MOUNTING_START:
      return {
        ...setupItem,
        options: getMountingOptions(setupItem.options, SetupItemTypeName.MOUNTING_START_HANDRAIL, SetupItemTypeName.MOUNTING_START_HANDRAIL_AND_GLASS),
      };
    case SetupItemName.MOUNTING_END:
      return {
        ...setupItem,
        options: getMountingOptions(setupItem.options, SetupItemTypeName.MOUNTING_END_HANDRAIL, SetupItemTypeName.MOUNTING_END_HANDRAIL_AND_GLASS),
      };
  }

  return setupItem;
};

export default class Parameters {
  constructor() {
    this.endpoint = '/parameters';
  }

  get() {
    return httpAdapter.fetch(this.endpoint)
      .then(httpAdapter.deserialize)
      .then((data) => {
        //TODO add edges item to parameters
        return [{
          id: SetupItemName.EDGES,
          name: 'edges',
          editable: true,
          options: [],
        },
        ...convertConfig(data)];
      });
  }

  handrailUpdateProxy(currentSetup, currentSetupList, handrailOptionId) {
    let updatedSetupList = currentSetupList;
    let updatedSetup = currentSetup;

    if (handrailOptionId === SetupItemTypeName.HANDRAIL_NO_RAIL) {
      updatedSetup[SetupItemName.MOUNTING_START].optionId = SetupItemTypeName.MOUNTING_START_NOTHING;
      updatedSetup[SetupItemName.MOUNTING_END].optionId = SetupItemTypeName.MOUNTING_END_NOTHING;
    } else if (handrailOptionId === SetupItemTypeName.HANDRAIL_WITH_RAIL) {
      // NOTE: code below recovers default handrail setup regardless of api setup list
      // it can cause inconsistency in setup
      updatedSetupList = currentSetupList.map((setupItem) => {
        if (setupItem.id === SetupItemName.MOUNTING_START || setupItem.id === SetupItemName.MOUNTING_END) {
          const { options } = setupList[setupItem.id];
          return {
            ...setupItem,
            options: Object.keys(options).map(key => ({ ...options[key], id: key })),
          }
        }

        return setupItem;
      });
    }

    return { setupList: updatedSetupList, setup: updatedSetup };
  }
}