import httpAdapter from './api-adapter';

export default class Parameters {
  constructor() {
    this.endpoint = '/contact/mail';
  }

  create(data) {
    return httpAdapter.create(this.endpoint, data)
      .then(httpAdapter.deserialize);
  }
}