import httpAdapter from './api-adapter';

export default class Transaction {
  constructor() {
    this.endpoint = '/transactions/project';
  }

  create(code, data) {
    return httpAdapter.create(`${this.endpoint}/${code}`, data)
      .then(httpAdapter.deserialize);
  }
}