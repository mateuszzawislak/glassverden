import axios from 'axios';

const DEFAULT_API_BASE_URL = '/api';

class RequestDebouncer {
  constructor() {
    this.requests = {};
  }

  get(...args) {
    const id = this.compare(...args);
    return { id, promise: this.requests[id] };
  }

  set(id, promise) {
    this.requests[id] = promise;
  }

  clear(id) {
    delete this.requests[id];
  }

  compare(path, params = {}) {
    let requestId = path;
    return Object.keys(params).sort().reduce((acc, key) => {
      return acc += `${key + params[key]}`;
    }, requestId);
  }
}

export default class ApiAdapter extends RequestDebouncer {
  constructor({ baseURL } = {}) {
    super();

    this.baseURL = process.env.DEV_SERVER === true && '//localhost:8080/api';
    this.http = axios.create({
      baseURL: baseURL || this.baseURL || DEFAULT_API_BASE_URL,
    });
  }

  fetch(path, params) {
    const { id, promise } = super.get(path, params);
    if (promise) {
      return promise;
    }

    const request = this.http.get(path, { params });
    super.set(id, request);

    return request
      .then((response) => {
        super.clear(id);
        return response;
      })
      .catch((error) => {
        super.clear(id);
        return this.errorDeserialize(error);
      });
  }

  create(path, body) {
    return this.http.post(path, body)
      .catch(this.errorDeserialize);
  }

  update(path, body) {
    return this.http.put(path, body)
      .catch(this.errorDeserialize);
  }

  deserialize(response) {
    if (response && response.data) {
      return response.data;
    }

    return {};
  }

  errorDeserialize(e = {}) {
    return Promise.reject(e.response && e.response.data || {});
  }
}