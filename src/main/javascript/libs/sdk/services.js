export default class Services {
  constructor() {
    this.services = {};
  }

  get(name) {
    const foundService = this.services[name];
    if (!foundService) {
      throw new Error(`Service ${name} does not exist!`);
    }

    return foundService;
  }

  add(name, constructor, props) {
    const foundService = this.services[name];
    if (foundService) {
      console.warn(`Service ${name} already exists!`);
      return foundService;
    }

    this.services[name] = new constructor(props);
    return this.services[name];
  }
}