export default {
  addToCart: 'Legg i handlekurv',
  copyrightNote: 'Copyright © GLASSVERDEN All rights reserved',
  continueProject: 'Fortsett prosjektet',
  deliveryTime: 'Antatt leveringsdato',
  email: 'E-post',
  followUs: 'Følg oss',
  paymentData: 'Org. nr',
  payments: 'Betalinger',
  phone: 'Mobile',
  price: 'Pris',
  railingType: 'Type glassrekkverk',
  railingTypeBar: 'Stolpefritt rekkverk med aluminiumskinne',
  railingTypePosts: 'Rekkverk med stolper',
  railingTypeRotules: 'Stolpefritt rekkverk med punktfester',
  startNewProject: 'Start mitt prosjekt',
  startProject: 'Start mitt prosjekt',
  unavailableRailingTypeText: 'Ta kontakt med oss for prosjektering av dette rekkverket',
  navbar: {
    home: 'Hjem',
    projects: 'Prosjekter',
    aboutCompany: 'Om oss',
    contact: 'Kontakt',
  },
  section: {
    operating: {
      title: 'Glassrekkverk som en modern løsning',
      header: 'Estetikk, holdbarhet og ikke minst sikkerhet',
      description: 'Glassrekkverk er preget av disse egenskapene og derfor bestemmer seg stadig flere å bruke dette i dagens arkitektur. Sammen med estetiske verdier skaper rekkverk et funksjonelt objekt. Takket være glass får man mye mer lys og et inntryk av plass forøkelse. Det fører til at små objekter er enda mer attraktive.',
    },
    designAndBuy: {
      title: 'Glassrekkverk etter ditt eget prosjekt',
      header: 'Du kan prosjektere ditt drømme rekkeverk fra A til Å',
      description: 'Det er så enkelt med oss! På vår nettside har du mulighet til å prosjektere og rotere i 3D ditt eget konseptet for rekkverk. Skritt etter skritt. Først ved å velge en av tre typer rekkverk, etterpå ved å definere alle parametere av glass, får du oversikt over ditt sluttprodukt. Du ser også pris - avhengig av alle valgene. Etter å ha oppnådd en tilfredsstillende effekt, er det bare å bestille. Da tar vi vare på resten! Avhengig av størrelsen på bestillingen, er produksjons- og leveringstid fra 3 til 6 uker.',
    },
    last: {
      header: 'Vi satser på fornøyde kunder',
      description: 'Vi tilbyr de beste produktene fra erfarne og pålitelige fabrikker i Polen. Konkurransedyktige priser. Vi tar vare på hvert prosjekt og gir profesjonelle råd. Transport over hele Norge. Vi gir deg 10 års garanti på våre produkter. Ikke nøl å ta kontakt med oss. Vi ser fram for et godt samarbeid.',
    },
    steps: {
      step1: 'Description of step 1 dolor sit amet',
      step2: 'Description of step 2 dapibus neque',
      step3: 'Description of step 3 tristique elit',
      step4: 'Description of step 4 risus quis eros.',
      step5: 'Description of step 5 mauris eu risus',
    }
  },
  page: {
    about: {
      offer: {
        header: 'Hva driver vi med',
        point1: 'vi er spesialister på salg av glassrekkverk',
        point2: 'hos oss kan du selv prosjektere ditt eget rekkverk',
        point3: 'vi gir deg profesjonelle råd',
        point4: 'konkurransedyktige priser og 10 års garanti',
      },
      whoWeAre: {
        header: 'Hvem er vi ',
        paragraph1: 'Selskapet Iwanski Gruppen ble stiftet i 2016. I starten var vår virksomhet basert på import av ulike byggevarer fra Polen. På grunn av stort etterspørsel og mange fornøyde kunder bestemte vi oss om å makredsføre vår eget merke. Derfor laget vi Glassverden - en moderne verktøy som gir deg mulighet til å prosjektere ditt eget drømme rekkverk. Du får opp en 3D modell på skjermen, samt pris avhengig av dine  valg. Vi garanterer den beste kvaliteten, trygg transport og gir deg råd både ved prosjektering og montering. ',
        paragraph2: 'Du kan stole på oss.',
      },
    },
    contact: {
      address: 'Adresse',
      errorMessage: 'An unknown error has occurred',
      name: 'Navn',
      messageContent: 'Din melding',
      sendMessage: 'Send melding',
      successMessage: 'The message has been sent successfully',
      error: {
        notBlank: 'Obligatorisk',
        maxSizeExceeded: 'Message is too long',
        notAnEmail: 'Incorrect format',
        unknown: 'Unknown error',
      },
    },
    railingConfigurator: {
      priceDescription: 'inkluderer moms og frakt',
      angle: {
        label: 'Vinkel',
        hint: 'Angle hint',
      },
      color: {
        label: 'Overflate',
        hint: 'Color hint',
        option: {
          shiny: 'Polert ståø',
          matt: 'Børstet stål',
        },
      },
      edges: {
        label: 'Mål',
        hint: 'Dimensions hint',
      },
      glassColor: {
        label: 'Glassfarge',
        hint: 'Glass color hint',
        option: {
          transparent: 'Klart',
          grey: 'Grå',
          brown: 'Brun',
          white: 'Frostet',
        },
      },
      glassThickness: {
        label: 'Glasstykkelse',
        hint: 'Glass thickness hint',
        option: {
          thickness44: '44.2 (tykkelse 8.76 mm)',
          thickness55: '55.2 (tykkelse 10.76 mm)',
          thickness66: '66.2 (tykkelse 12.76 mm)',
        },
      },
      glassType: {
        label: 'Glasstype',
        hint: 'Glass type hint',
        option: {
          normal: 'Normal',
          tempered: 'Herdet',
        },
      },
      normal: { hint: 'Hint normal' },
      tempered: { hint: 'Hint tempered' },
      handrail: {
        label: 'Håndløper',
        hint: 'Handrail hint',
        option: {
          handrailNoRail: 'Uten håndløper',
          handrailWithRail: 'Med håndløper',
        },
      },
      height: {
        label: 'Rekkverkshøyde',
        hint: 'Height hint',
        option: {
          height900: '900 mm',
          height1000: '1000 mm',
          height1200: '1200 mm',
        },
      },
      height900: { hint: 'Hint height900' },
      height1000: { hint: 'Hint height1000' },
      height1200: { hint: 'Hint height1200' },
      length: {
        label: 'Lengde',
        hint: 'Length hint',
      },
      mounting: {
        label: 'Festemåte',
        hint: 'Mounting hint',
        option: {
          mountingGround: 'Ovenpå',
          mountingSide120: 'Utenpå 120 mm',
          mountingSide135: 'Utenpå 135 mm',
          mountingSide150: 'Utenpå 150 mm',
          mountingSide165: 'Utenpå 165 mm',
          mountingSide180: 'Utenpå 180 mm',
          mountingSide195: 'Utenpå 195 mm',
          mountingSide210: 'Utenpå 210 mm',
          mountingSide225: 'Utenpå 225 mm',
          mountingSide240: 'Utenpå 240 mm',
          mountingSide255: 'Utenpå 255 mm',
        },
      },
      mountingGround: { hint: 'Hint mountingGround' },
      mountingSide120: { hint: 'Hint to the side 120mm' },
      mountingSide135: { hint: 'Hint to the side 135mm' },
      mountingSide150: { hint: 'Hint to the side 150mm' },
      mountingSide165: { hint: 'Hint to the side 165mm' },
      mountingSide180: { hint: 'Hint to the side 180mm' },
      mountingSide195: { hint: 'Hint to the side 195mm' },
      mountingSide210: { hint: 'Hint to the side 210mm' },
      mountingSide225: { hint: 'Hint to the side 225mm' },
      mountingSide240: { hint: 'Hint to the side 240mm' },
      mountingSide255: { hint: 'Hint to the side 255mm' },
      mountingEnd: {
        label: 'Festemåte i avslutning',
        hint: 'Mounting End hint',
        option: {
          glass: 'Inntil vegg',
          nothing: 'Frittstående stolpe',
          handrail: 'Håndløper inntil vegg',
          handrailAndGlass: 'Inntil vegg uten stolpe',
        },
      },
      mountingStart: {
        label: 'Festemåte i begynnelse',
        hint: 'Mounting Start hint',
        option: {
          glass: 'Inntil vegg',
          nothing: 'Frittstående stolpe',
          handrail: 'Håndløper inntil vegg',
          handrailAndGlass: 'Inntil vegg uten stolpe',
        },
      },
      profile: {
        label: 'Profil',
        hint: 'Profile hint',
        option: {
          round: 'Rundet',
          square: 'Firkantet',
        },
      },
      segmentsNumber: {
        label: 'Antall segmenter',
        hint: 'Segments amount hint',
      },
      steelClass: {
        label: 'Ståltyper',
        hint: 'Steel Class hint',
        option: {
          AISI316: 'AISI316',
          AISI304: 'AISI304',
        },
      },
      AISI316: { hint: 'Hint AISI316' },
      AISI304: { hint: 'Hint AISI304' },
      error: {
        edgeTooLong: 'Too long edge',
        edgeTooShort: 'Too short edge',
        invalidProjectStatus: 'Cannot edit completed project',
        maxAngle: 'Incorrect angle (max 359°)',
        minValue: 'Incorrect value (min 1)',
        oneSegmentRequired: 'You cannot divide a glass shorter than 800 mm',
        segmentTooLong: 'Not enought segments',
        segmentTooShort: 'Too many segments',
        unknown: 'Unknown error',
      },
    },
    projects: {
      section: {
        first: {
          header: 'Stolpefritt rekkverk med aluminiumskinne',
          description: 'Denne typen rekkverk er meget eksklusiv på grunn av stor gjennomsiktighet av glass. Effekten kan oppnås ved å montere glass i aluminiumskinne istedenfor å bruke stolper som forhindrer utsikten. Et enda mer spektakulært resultat får man ved å montere skinne i undergulvet. Vi tilbyr 4 farger av både herdet og normalt glass.',
        },
        second: {
          header: 'Rekkverk med stolper',
          description: 'Rekkverk med stolper er mest populært. Det er preget av enkel montering og stabil konstruksjon. Alle horisontale og vertikale krefter virker på stolper med senteravstand fra 90 til 130 cm. Disse er knyttet sammen med håndløper. Man kan også montere rekkverk uten noen håndløper. Vi disponerer forskjellige tykkelser, typer og farger av glass, samt mange former, farger og festemåte av stolper. Slikt kan man få et fint, tilpasset produkt til en rimelig pris.',
        },
        third: {
          header: 'Stolpefritt rekkverk med punktfester',
          description: 'Det kalles også rekkverk i helt glass, fordi bortsett fra glass og punktfester finnes det ikke andre elementer i konstruksjonen. Rekkverket er montert med hjelp av punkt- eller linjekontakter, som festes i forboret glass. Montering krever mye presisjon. Resultatet er alltid spektakulær. Oftest brukes som beskyttelse til trapp.',
        },
      },
    },
    summary: {
      city: 'By',
      flatNumber: 'Husnummer',
      emptyProject: 'Project does not exist!',
      goToHomePage: 'Go to home page',
      name: 'Fornavn',
      no: 'No.',
      or: 'or',
      order: 'Order',
      orderCompletedHeader: 'Order completed header',
      orderCompletedDescription: 'Order completed description',
      orderDataHeader: 'Bestillingsdata',
      postalCode: 'Postnummer',
      projectSummaryHeader: 'Oppsumering',
      street: 'Gate',
      surname: 'Etternavn',
      error: {
        notBlank: 'Obligatorisk',
        maxSizeExceeded: 'Message is too long',
        notAnEmail: 'Incorrect format',
        unknown: 'Unknown error',
      },
    },
  },
};