import en from './en';
import no from './no';
import pl from './pl';

export default {
  en,
  no,
  pl,
};