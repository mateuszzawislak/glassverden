import Vue from 'vue';
import VueI18Next from '@panter/vue-i18next';
import Cookie from 'js-cookie';
import i18next from 'i18next';
import locales from './locales';

const DEFAULT_LANGUAGE = 'no';
const LANGUAGE_COOKIE_EXPIRE_TIME = 365; //[d]

class LanguageManager {
  constructor(i18n) {
    this.i18n = i18n;
  }

  get current() {
    return this.i18n.i18next.language;
  }

  parseUrl(route) {
    const { lang } = route.query;
    if (lang) {
      this.setLanguage(lang);
      const newQuery = { ...route.query };
      delete newQuery.lang;

      return newQuery;
    }
  }

  setLanguage(lang) {
    if (this.current !== lang) {
      Cookie.set('language', lang, { expires: LANGUAGE_COOKIE_EXPIRE_TIME });
      this.i18n.i18next.changeLanguage(lang);
    }
  }
}

Vue.use(VueI18Next);

i18next.init({
  lng: Cookie.get('language') || DEFAULT_LANGUAGE,
  resources: {
    pl: {
      translation: locales.pl,
    },
    en: {
      translation: locales.en,
    },
    no: {
      translation: locales.no,
    }
  }
});

const i18n = new VueI18Next(i18next);

export default new LanguageManager(i18n);