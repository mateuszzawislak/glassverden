package no.glassverden.transaction.model;

public enum TransactionStatus {
    NEW,
    PAID,
    REJECTED;
}
