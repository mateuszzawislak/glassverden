package no.glassverden.transaction.model;

import no.glassverden.project.model.Project;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Set;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_code", nullable = false)
    private Project project;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionStatus status;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "transaction", cascade = CascadeType.ALL)
    @OrderBy("id")
    private Set<TransactionOption> transactionOptions;

    @Column(nullable = false, name = "net_price", precision = 15, scale = 5)
    private BigDecimal netPrice;

    @Column(nullable = false, name = "gross_price", precision = 15, scale = 5)
    private BigDecimal grossPrice;

    @Column(nullable = false, name = "delivery_price", precision = 15, scale = 5)
    private BigDecimal deliveryPrice;

    @Column(nullable = false, name = "tax", precision = 5, scale = 2)
    private BigDecimal tax;

    @Column(name = "created_at", nullable = false)
    private ZonedDateTime createdAt;

    @Column(name = "modified_at", nullable = false)
    private ZonedDateTime modifiedAt;

    @PrePersist
    private void prePersist() {
        createdAt = ZonedDateTime.now();
        modifiedAt = ZonedDateTime.now();
    }

    @PreUpdate
    private void preUpdate() {
        modifiedAt = ZonedDateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public Set<TransactionOption> getTransactionOptions() {
        return transactionOptions;
    }

    public void setTransactionOptions(Set<TransactionOption> transactionOptions) {
        this.transactionOptions = transactionOptions;
    }

    public BigDecimal getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(BigDecimal netPrice) {
        this.netPrice = netPrice;
    }

    public BigDecimal getGrossPrice() {
        return grossPrice;
    }

    public void setGrossPrice(BigDecimal grossPrice) {
        this.grossPrice = grossPrice;
    }

    public BigDecimal getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(BigDecimal deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }
}
