package no.glassverden.transaction.model;

import no.glassverden.parameter.model.ParameterOption;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "transaction_option")
public class TransactionOption {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transaction_id", nullable = false)
    private Transaction transaction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parameter_option_code", nullable = false)
    private ParameterOption parameterOption;

    @Column(nullable = false, name = "price_weight", precision = 15, scale = 5)
    private BigDecimal priceWeight;

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public void setParameterOption(ParameterOption parameterOption) {
        this.parameterOption = parameterOption;
    }

    public ParameterOption getParameterOption() {
        return parameterOption;
    }

    public void setPriceWeight(BigDecimal priceWeight) {
        this.priceWeight = priceWeight;
    }
}
