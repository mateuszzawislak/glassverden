package no.glassverden.transaction.serializer;

import no.glassverden.transaction.dto.TransactionDto;
import no.glassverden.transaction.model.Transaction;

public class TransactionSerializer {

    public static TransactionDto toDto(Transaction transaction) {
        TransactionDto dto = new TransactionDto();

        dto.setId(transaction.getId());
        dto.setStatus(transaction.getStatus());
        dto.setNetPrice(transaction.getNetPrice());
        dto.setGrossPrice(transaction.getGrossPrice());
        dto.setTax(transaction.getTax());

        return dto;
    }

}
