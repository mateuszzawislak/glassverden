package no.glassverden.transaction.controller;

import no.glassverden.project.deserializer.ContactDeserializer;
import no.glassverden.transaction.dto.ContactDto;
import no.glassverden.transaction.dto.TransactionDto;
import no.glassverden.transaction.serializer.TransactionSerializer;
import no.glassverden.transaction.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/transactions")
public class TransactionApiController {

    private TransactionService transactionService;

    @Autowired
    public TransactionApiController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @RequestMapping(path = "project/{projectCode}", method = RequestMethod.POST)
    public ResponseEntity<TransactionDto> create(@PathVariable String projectCode,
                                                 @RequestBody @Valid ContactDto contact) {
        return ResponseEntity.ok(TransactionSerializer.toDto(
                transactionService.create(projectCode, ContactDeserializer.fromDto(contact))));
    }

}
