package no.glassverden.transaction.dto;

import static no.glassverden.web.validation.ValidationErrorMessage.NOT_AN_EMAIL;
import static no.glassverden.web.validation.ValidationErrorMessage.NOT_BLANK;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class ContactDto {

    @NotBlank(message = NOT_BLANK)
    @Email(message = NOT_AN_EMAIL)
    private String email;

    @NotBlank(message = NOT_BLANK)
    private String phone;

    @NotBlank(message = NOT_BLANK)
    private String name;

    @NotBlank(message = NOT_BLANK)
    private String surname;

    @NotBlank(message = NOT_BLANK)
    private String postalCode;

    @NotBlank(message = NOT_BLANK)
    private String city;

    @NotBlank(message = NOT_BLANK)
    private String street;

    @NotBlank(message = NOT_BLANK)
    private String flatNumber;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }
}
