package no.glassverden.transaction.service;

import no.glassverden.locale.service.LocaleService;
import no.glassverden.mail.service.MailService;
import no.glassverden.mail.template.MailTemplate;
import no.glassverden.mail.template.TemplateResource;
import no.glassverden.parameter.model.ParameterOption;
import no.glassverden.project.model.Project;
import no.glassverden.project.serializer.ContactSerializer;
import no.glassverden.transaction.model.Transaction;
import no.glassverden.transaction.model.TransactionOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OrderConfirmationMailService {

    private LocaleService localeService;

    private MailService mailService;

    @Value("#{'${mail.admin.contacts}'.split(',')}")
    private List<String> adminContacts;

    @Autowired
    public OrderConfirmationMailService(LocaleService localeService, MailService mailService) {
        this.localeService = localeService;
        this.mailService = mailService;
    }

    public void sendOrderConfirmationMail(Project project, Transaction transaction) {

        Map<String, Object> contentParams = new HashMap() {{
            put("projectCode", project.getCode());
            put("selectedOptions", getSelectedOptions(transaction, new Locale(project.getLanguage().getCode())));
            put("contact", ContactSerializer.toDto(project.getContact()));
            put("price", transaction.getGrossPrice());
        }};

        MailTemplate mail = new MailTemplate.Builder()
                .subject(localeService.getMessage("mail.order.subject"))
                .contentTemplate(TemplateResource.ORDER_CONFIRMATION)
                .contentParams(contentParams)
                .recipient(project.getContact().getEmail())
                .hiddenRecipients(adminContacts)
                .build();

        mailService.sendMail(mail);
    }

    private List<String> getSelectedOptions(Transaction transaction, Locale locale) {
        return transaction.getTransactionOptions().stream()
                .map(TransactionOption::getParameterOption)
                .map(ParameterOption::getCode)
                .map(Enum::toString)
                .map(option -> localeService.getMessage(option, locale))
                .collect(Collectors.toList());
    }
}
