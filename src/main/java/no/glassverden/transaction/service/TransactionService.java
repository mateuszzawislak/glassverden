package no.glassverden.transaction.service;

import static no.glassverden.web.validation.ValidationErrorMessage.INVALID_TRANSACTION_STATUS;

import java.util.Comparator;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import no.glassverden.parameter.model.ParameterOption;
import no.glassverden.project.model.Contact;
import no.glassverden.project.model.Project;
import no.glassverden.project.model.ProjectStatus;
import no.glassverden.project.repository.ProjectRepository;
import no.glassverden.project.service.ProjectService;
import no.glassverden.project.service.price.PriceCalculationStrategy;
import no.glassverden.transaction.model.Transaction;
import no.glassverden.transaction.model.TransactionOption;
import no.glassverden.transaction.model.TransactionStatus;
import no.glassverden.web.validation.exception.UnsupportedOperationException;

@Service
public class TransactionService {

    private ProjectService projectService;

    private ProjectRepository projectRepository;

    private PriceCalculationStrategy priceCalculationStrategy;

    private OrderConfirmationMailService orderConfirmationMailService;

    @Autowired
    public TransactionService(ProjectService projectService,
                              ProjectRepository projectRepository,
                              PriceCalculationStrategy priceCalculationStrategy,
                              OrderConfirmationMailService orderConfirmationMailService) {
        this.projectService = projectService;
        this.projectRepository = projectRepository;
        this.priceCalculationStrategy = priceCalculationStrategy;
        this.orderConfirmationMailService = orderConfirmationMailService;
    }

    public Transaction create(String projectCode, Contact contact) {
        Project project = projectService.get(projectCode);
        projectService.validateAndChangeProjectStatus(project, ProjectStatus.COMPLETED);
        validateAndCloseExistingTransactions(project);
        updateContactInfo(project, contact);

        Transaction transaction = createTransactionForProject(project);
        project.getTransactions().add(transaction);
        transaction = getLatestTransaction(projectRepository.save(project));

        orderConfirmationMailService.sendOrderConfirmationMail(project, transaction);

        return transaction;
    }

    public void validateAndCloseExistingTransactions(Project project) {
        project.getTransactions().stream().forEach(transaction -> {
            if (transaction.getStatus() == TransactionStatus.PAID) {
                throw new UnsupportedOperationException(INVALID_TRANSACTION_STATUS);
            }
            transaction.setStatus(TransactionStatus.REJECTED);
        });
    }

    private void updateContactInfo(Project project, Contact contact) {
        if (project.getContact() == null) {
            Contact newContact = new Contact();
            newContact.setProject(project);
            project.setContact(newContact);
        }

        Contact contactInDb = project.getContact();
        contactInDb.setEmail(contact.getEmail());
        contactInDb.setPhone(contact.getPhone());
        contactInDb.setName(contact.getName());
        contactInDb.setSurname(contact.getSurname());
        contactInDb.setPostalCode(contact.getPostalCode());
        contactInDb.setCity(contact.getCity());
        contactInDb.setStreet(contact.getStreet());
        contactInDb.setFlatNumber(contact.getFlatNumber());
    }

    private Transaction createTransactionForProject(Project project) {
        Transaction transaction = new Transaction();
        transaction.setProject(project);
        transaction.setStatus(TransactionStatus.NEW);
        transaction.setDeliveryPrice(priceCalculationStrategy.getDeliveryPrice(project));
        transaction.setTax(priceCalculationStrategy.getTax(project));
        transaction.setNetPrice(priceCalculationStrategy.getNetPrice(project));
        transaction.setGrossPrice(priceCalculationStrategy.getGrossPrice(project));

        transaction.setTransactionOptions(project.getParameterOptions().stream()
                .map(selectedOption -> createTransactionOption(selectedOption, transaction))
                .collect(Collectors.toSet()));

        return transaction;
    }

    private TransactionOption createTransactionOption(ParameterOption parameterOption, Transaction transaction) {
        TransactionOption transactionOption = new TransactionOption();

        transactionOption.setTransaction(transaction);
        transactionOption.setParameterOption(parameterOption);
        transactionOption.setPriceWeight(priceCalculationStrategy.getParameterOptionPriceWeight(parameterOption));

        return transactionOption;
    }

    private Transaction getLatestTransaction(Project project) {
        return project.getTransactions().stream()
                .sorted(Comparator.comparing(Transaction::getCreatedAt).reversed())
                .findFirst()
                .orElse(null);
    }
}
