package no.glassverden.core.validation;

public interface Validator<T> {

    public void validate(T obj);

}
