package no.glassverden.web.validation.dto;

import java.util.ArrayList;
import java.util.List;

public class ValidationResultDto {

    private List<ValidationErrorDto> errors;

    public ValidationResultDto() {
        errors = new ArrayList<>();
    }

    public ValidationResultDto(String errorCode) {
        errors = new ArrayList<>();
        addError(errorCode);
    }

    public void addError(String errorCode) {
        errors.add(new ValidationErrorDto(null, errorCode));
    }

    public void addError(String filedName, String errorCode) {
        errors.add(new ValidationErrorDto(filedName, errorCode));
    }

    public List<ValidationErrorDto> getErrors() {
        return errors;
    }
}
