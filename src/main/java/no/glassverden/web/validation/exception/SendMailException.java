package no.glassverden.web.validation.exception;

public class SendMailException extends RuntimeException {

    public SendMailException(String message) {
        super(message);
    }
}