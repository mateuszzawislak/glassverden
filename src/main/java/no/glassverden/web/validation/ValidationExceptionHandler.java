package no.glassverden.web.validation;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import no.glassverden.web.validation.dto.ValidationResultDto;
import no.glassverden.web.validation.exception.IllegalInputException;
import no.glassverden.web.validation.exception.ObjectNotFoundException;
import no.glassverden.web.validation.exception.SendMailException;
import no.glassverden.web.validation.exception.UnsupportedOperationException;

@ControllerAdvice
public class ValidationExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ValidationResultDto result = new ValidationResultDto();

        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            result.addError(fieldName, errorMessage);
        });
        return new ResponseEntity(result, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    public ResponseEntity<Object> handleObjectNotFound(ObjectNotFoundException ex) {
        return new ResponseEntity(new ValidationResultDto(ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UnsupportedOperationException.class)
    public ResponseEntity<Object> handleUnsupportedOperationException(UnsupportedOperationException ex) {
        return new ResponseEntity(new ValidationResultDto(ex.getMessage()), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(SendMailException.class)
    public ResponseEntity<Object> handleSMTPAddressFailedExceptionException(SendMailException ex) {
        return new ResponseEntity(new ValidationResultDto(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalInputException.class)
    public ResponseEntity<Object> handleIllegalInputException(IllegalInputException ex) {
        return new ResponseEntity(new ValidationResultDto(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
