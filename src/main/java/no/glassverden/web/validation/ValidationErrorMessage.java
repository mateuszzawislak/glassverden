package no.glassverden.web.validation;

public class ValidationErrorMessage {
    public static final String EDGE_TOO_LONG = "EDGE_TOO_LONG";
    public static final String EDGE_TOO_SHORT = "EDGE_TOO_SHORT";
    public static final String INVALID_EMAIL = "INVALID_EMAIL";
    public static final String INVALID_PROJECT_STATUS = "INVALID_PROJECT_STATUS";
    public static final String INVALID_TRANSACTION_STATUS = "INVALID_TRANSACTION_STATUS";
    public static final String MAX_SIZE_EXCEEDED = "MAX_SIZE_EXCEEDED";
    public static final String MAX_359 = "MAX_359";
    public static final String MIN_0 = "MIN_0";
    public static final String MIN_1 = "MIN_1";
    public static final String NOT_AN_EMAIL = "NOT_AN_EMAIL";
    public static final String NOT_BLANK = "NOT_BLANK";
    public static final String NOT_EMPTY = "NOT_EMPTY";
    public static final String NOT_NULL = "NOT_NULL";
    public static final String ONE_SEGMENT_REQUIRED = "ONE_SEGMENT_REQUIRED";
    public static final String PARAMETER_NOT_FOUND = "PARAMETER_NOT_FOUND";
    public static final String PROJECT_NOT_FOUND = "PROJECT_NOT_FOUND";
    public static final String SEGMENT_TOO_LONG = "SEGMENT_TOO_LONG";
    public static final String SEGMENT_TOO_SHORT = "SEGMENT_TOO_SHORT";
    public static final String UNKNOWN_ERROR_DURING_SENDING_EMAIL = "UNKNOWN_ERROR_DURING_SENDING_EMAIL";
}
