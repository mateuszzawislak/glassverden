package no.glassverden.web.documentation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DocumentationController {

    @RequestMapping(value = { "documentation" }, method = RequestMethod.GET)
    public String swagger() {
        return "redirect:/static/swagger-ui.html";
    }

}
