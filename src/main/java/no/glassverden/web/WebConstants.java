package no.glassverden.web;

public class WebConstants {

    public static final String LOCALE_COOKIE_NAME = "language";
    public static final Integer LOCALE_COOKIE_MAX_AGE = 365 * 24 * 60 * 60; // [s]
    public static final String LOCALE_PARAM_NAME = "lang";

}
