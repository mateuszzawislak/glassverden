package no.glassverden.contact.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import no.glassverden.locale.service.LocaleService;
import no.glassverden.mail.service.MailService;
import no.glassverden.mail.template.MailTemplate;
import no.glassverden.mail.template.TemplateResource;

@Service
public class ContactMailService {

    private LocaleService localeService;

    private MailService mailService;

    @Value("${spring.mail.username}")
    private String username;

    @Value("#{'${mail.admin.contacts}'.split(',')}")
    private List<String> adminContacts;

    @Autowired
    public ContactMailService(LocaleService localeService, MailService mailService) {
        this.localeService = localeService;
        this.mailService = mailService;
    }

    public void sendContactMail(String content, String name, String clientEmail, String phoneNumber) {
        mailService.sendMail(buildContactAdminMail(content, name, clientEmail, phoneNumber));
        mailService.sendMail(buildContactClientMail(content, name, clientEmail, phoneNumber));
    }

    private MailTemplate buildContactAdminMail(String content, String name, String clientEmail, String phoneNumber) {
        Map<String, Object> contentParams = new HashMap() {{
            put("name", name);
            put("clientEmail", clientEmail);
            put("content", content);
            put("phoneNumber", phoneNumber);
        }};

        return new MailTemplate.Builder()
                .subject(localeService.getMessage("mail.contact.form.admin.subject"))
                .contentTemplate(TemplateResource.CONTACT_EMAIL_ADMIN)
                .contentParams(contentParams)
                .recipient(username)
                .hiddenRecipients(adminContacts)
                .build();
    }

    private MailTemplate buildContactClientMail(String content, String name, String clientEmail, String phoneNumber) {
        Map<String, Object> contentParams = new HashMap() {{
            put("name", name);
            put("clientEmail", clientEmail);
            put("content", content);
            put("phoneNumber", phoneNumber);
        }};

        return new MailTemplate.Builder()
                .subject(localeService.getMessage("mail.contact.form.client.subject"))
                .contentTemplate(TemplateResource.CONTACT_EMAIL_CLIENT)
                .contentParams(contentParams)
                .recipient(clientEmail)
                .build();
    }
}
