package no.glassverden.contact.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import no.glassverden.contact.dto.ContactMailDto;
import no.glassverden.contact.service.ContactMailService;

@RestController
@RequestMapping("api/contact")
public class ContactApiController {

    private ContactMailService contactMailService;

    @Autowired
    public ContactApiController(ContactMailService contactMailService) {
        this.contactMailService = contactMailService;
    }

    @RequestMapping(path = "mail", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void sendMail(@Valid @RequestBody ContactMailDto mailDto) {
        contactMailService.sendContactMail(mailDto.getContent(), mailDto.getName(), mailDto.getMail(),
                mailDto.getPhone());
    }
}
