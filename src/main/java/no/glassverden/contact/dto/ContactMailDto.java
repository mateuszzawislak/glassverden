package no.glassverden.contact.dto;

import static no.glassverden.web.validation.ValidationErrorMessage.MAX_SIZE_EXCEEDED;
import static no.glassverden.web.validation.ValidationErrorMessage.NOT_AN_EMAIL;
import static no.glassverden.web.validation.ValidationErrorMessage.NOT_BLANK;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

public class ContactMailDto {

    @NotBlank(message = NOT_BLANK)
    private String name;

    @NotBlank(message = NOT_BLANK)
    @Email(message = NOT_AN_EMAIL)
    private String mail;

    @NotBlank(message = NOT_BLANK)
    private String phone;

    @NotBlank(message = NOT_BLANK)
    @Length(max = 500, message = MAX_SIZE_EXCEEDED)
    private String content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
