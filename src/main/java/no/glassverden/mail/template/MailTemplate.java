package no.glassverden.mail.template;

import java.util.List;
import java.util.Map;

public class MailTemplate {

    private String subject;

    private String recipient;

    private TemplateResource contentTemplate;

    private Map<String, Object> contentParams;

    private List<String> hiddenRecipients;

    public String getSubject() {
        return subject;
    }

    public String getRecipient() {
        return recipient;
    }

    public TemplateResource getContentTemplate() {
        return contentTemplate;
    }

    public Map<String, Object> getContentParams() {
        return contentParams;
    }

    public List<String> getHiddenRecipients() {
        return hiddenRecipients;
    }

    @Override
    public String toString() {
        return "MailTemplate{" +
                "subject='" + subject + '\'' +
                ", recipient='" + recipient + '\'' +
                ", contentTemplate=" + contentTemplate +
                ", contentParams=" + contentParams +
                ", hiddenRecipients=" + hiddenRecipients +
                '}';
    }

    public static final class Builder {

        private String subject;
        private String recipient;
        private TemplateResource contentTemplate;
        private Map<String, Object> contentParams;
        private List<String> hiddenRecipients;

        public Builder subject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder recipient(String recipient) {
            this.recipient = recipient;
            return this;
        }

        public Builder contentTemplate(TemplateResource contentTemplate) {
            this.contentTemplate = contentTemplate;
            return this;
        }

        public Builder contentParams(Map<String, Object> contentParams) {
            this.contentParams = contentParams;
            return this;
        }

        public Builder hiddenRecipients(List<String> hiddenRecipients) {
            this.hiddenRecipients = hiddenRecipients;
            return this;
        }

        public MailTemplate build() {
            MailTemplate mailTemplate = new MailTemplate();

            mailTemplate.subject = this.subject;
            mailTemplate.recipient = this.recipient;
            mailTemplate.contentTemplate = this.contentTemplate;
            mailTemplate.contentParams = this.contentParams;
            mailTemplate.hiddenRecipients = this.hiddenRecipients;

            return mailTemplate;
        }
    }
}
