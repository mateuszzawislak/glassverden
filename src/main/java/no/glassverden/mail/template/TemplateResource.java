package no.glassverden.mail.template;

public enum TemplateResource {
    CONTACT_EMAIL_ADMIN("mail/contact-email-admin.html"),
    CONTACT_EMAIL_CLIENT("mail/contact-email-client.html"),
    ORDER_CONFIRMATION("mail/order-confirmation.html");

    private final String path;

    TemplateResource(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
