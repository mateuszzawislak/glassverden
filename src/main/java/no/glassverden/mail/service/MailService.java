package no.glassverden.mail.service;

import java.util.Arrays;
import java.util.Map;

import javax.jms.Queue;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.sun.mail.smtp.SMTPAddressFailedException;

import no.glassverden.mail.queue.QueueConfiguration;
import no.glassverden.mail.template.MailTemplate;
import no.glassverden.mail.template.TemplateResource;
import no.glassverden.web.validation.ValidationErrorMessage;
import no.glassverden.web.validation.exception.SendMailException;

@Service
public class MailService {

    private Logger logger = LoggerFactory.getLogger(MailService.class);

    private JavaMailSender emailSender;

    private TemplateEngine templateEngine;

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private Queue queue;

    @Value("${spring.mail.username}")
    private String username;

    @Autowired
    public MailService(JavaMailSender emailSender, TemplateEngine templateEngine) {
        this.emailSender = emailSender;
        this.templateEngine = templateEngine;
    }

    public void sendMail(MailTemplate mailTemplate) {
        logger.info("Mail to be send {}", mailTemplate);
        this.jmsMessagingTemplate.convertAndSend(this.queue, mailTemplate);
    }

    @JmsListener(destination = QueueConfiguration.MAIL_QUEUE_NAME)
    private void receiveQueue(MailTemplate mailTemplate) throws MessagingException {
        logger.info("Sending mail {}", mailTemplate);

        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom(username);
        helper.setTo(mailTemplate.getRecipient());
        if (mailTemplate.getHiddenRecipients() != null) {
            helper.setBcc(mailTemplate.getHiddenRecipients().stream().toArray(String[]::new));
        }
        helper.setSubject(mailTemplate.getSubject());

        String content = generateTemplateContent(mailTemplate.getContentTemplate(), mailTemplate.getContentParams());
        helper.setText(content, true);

        try {
            emailSender.send(helper.getMimeMessage());
        } catch (MailSendException ex) {
            logger.error("Exception while sending mail", ex);
            handleSendException(ex);
        }

        logger.info("Sent mail {}", mailTemplate);
    }

    private String generateTemplateContent(TemplateResource template, Map<String, Object> variables) {
        Context context = new Context();
        context.setVariables(variables);
        context.setLocale(LocaleContextHolder.getLocale());
        return templateEngine.process(template.getPath(), context);
    }

    private void handleSendException(MailSendException ex) {
        Arrays.stream(ex.getMessageExceptions())
                .filter(exception -> exception instanceof SendFailedException)
                .map(messageException -> (SendFailedException) messageException)
                .map(SendFailedException::getNextException)
                .filter(exception -> exception instanceof SMTPAddressFailedException)
                .findAny()
                .ifPresent((s) -> {
                    throw new SendMailException(ValidationErrorMessage.INVALID_EMAIL);
                });
        throw new SendMailException(ValidationErrorMessage.UNKNOWN_ERROR_DURING_SENDING_EMAIL);
    }
}
