package no.glassverden.mail.queue;

import javax.jms.Queue;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QueueConfiguration {

    public static final String MAIL_QUEUE_NAME = "mail.queue";

    @Bean
    public Queue queue() {
        return new ActiveMQQueue(MAIL_QUEUE_NAME);
    }
}
