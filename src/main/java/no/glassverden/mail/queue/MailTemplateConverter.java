package no.glassverden.mail.queue;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import no.glassverden.mail.template.MailTemplate;

@Component
public class MailTemplateConverter implements MessageConverter {

    private static final Logger logger = LoggerFactory.getLogger(MailTemplateConverter.class);

    private ObjectMapper mapper;

    public MailTemplateConverter() {
      mapper = new ObjectMapper();
    }

    @Override
    public Message toMessage(Object object, Session session) throws JMSException {
        MailTemplate mailTemplate = (MailTemplate) object;

        String payload = null;
        try {
            payload = mapper.writeValueAsString(mailTemplate);
        } catch (JsonProcessingException e) {
            logger.error("Error converting from mailTemplate", e);
        }

        TextMessage message = session.createTextMessage();
        message.setText(payload);

        return message;
    }

    @Override
    public Object fromMessage(Message message) throws JMSException {
        TextMessage textMessage = (TextMessage) message;
        String payload = textMessage.getText();

        MailTemplate mailTemplate = null;
        try {
            mailTemplate = mapper.readValue(payload, MailTemplate.class);
        } catch (Exception e) {
            logger.error("Error converting to mailTemplate", e);
        }

        return mailTemplate;
    }
}
