package no.glassverden;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    private Logger logger = LoggerFactory.getLogger(MainController.class);

    @RequestMapping(value = { "*" }, method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(path = "robots.txt", method = RequestMethod.GET)
    public void robots(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.getWriter().write("User-agent: *\nAllow: /\n");
        } catch (IOException e) {
            logger.error("Problem with serving robots.txt file {}", e);
        }
    }

}
