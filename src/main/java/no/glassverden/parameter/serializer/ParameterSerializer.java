package no.glassverden.parameter.serializer;

import no.glassverden.parameter.dto.ParameterDto;
import no.glassverden.parameter.dto.ParameterOptionDto;
import no.glassverden.parameter.model.Parameter;
import no.glassverden.parameter.model.ParameterOption;

import java.util.stream.Collectors;

public class ParameterSerializer {

    public static ParameterDto toDto(Parameter parameter) {
        ParameterDto dto = new ParameterDto();

        dto.setCode(parameter.getCode());
        dto.setRank(parameter.getRank());
        dto.setParameterOptions(parameter.getParameterOptions().stream()
                .map(ParameterSerializer::toDto)
                .collect(Collectors.toList()));

        return dto;
    }

    public static ParameterOptionDto toDto(ParameterOption option) {
        ParameterOptionDto dto = new ParameterOptionDto();

        dto.setCode(option.getCode());
        dto.setRank(option.getRank());

        return dto;
    }
}
