package no.glassverden.parameter.dto;

import no.glassverden.parameter.model.ParameterType;

import java.util.List;

public class ParameterDto {

    private ParameterType code;

    private List<ParameterOptionDto> parameterOptions;

    private int rank;

    public ParameterType getCode() {
        return code;
    }

    public void setCode(ParameterType code) {
        this.code = code;
    }

    public List<ParameterOptionDto> getParameterOptions() {
        return parameterOptions;
    }

    public void setParameterOptions(List<ParameterOptionDto> parameterOptions) {
        this.parameterOptions = parameterOptions;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
