package no.glassverden.parameter.dto;

import static no.glassverden.web.validation.ValidationErrorMessage.NOT_NULL;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import no.glassverden.parameter.model.ParameterOptionType;
import no.glassverden.project.validation.ProjectUpdateValidationGroup;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class ParameterOptionDto {

    @NotNull(message = NOT_NULL, groups = ProjectUpdateValidationGroup.class)
    private ParameterOptionType code;

    private Integer rank;

    public ParameterOptionType getCode() {
        return code;
    }

    public void setCode(ParameterOptionType code) {
        this.code = code;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }
}
