package no.glassverden.parameter.service;

import no.glassverden.parameter.model.Parameter;
import no.glassverden.parameter.model.ParameterOption;
import no.glassverden.parameter.model.ParameterOptionType;
import no.glassverden.parameter.repository.ParameterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ParameterService {

    private ParameterRepository parameterRepository;

    @Autowired
    public ParameterService(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    public List<Parameter> listActive() {
        return parameterRepository.listActive();
    }

    public Set<ParameterOptionType> listActiveOptionTypes() {
        return parameterRepository.listActive().stream()
                .flatMap(parameter -> parameter.getParameterOptions().stream())
                .map(ParameterOption::getCode)
                .collect(Collectors.toSet());
    }
}
