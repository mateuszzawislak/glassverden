package no.glassverden.parameter.repository;

import no.glassverden.parameter.model.Parameter;
import no.glassverden.parameter.model.ParameterType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface ParameterRepository extends Repository<Parameter, ParameterType> {

    @Query("SELECT DISTINCT p FROM Parameter p " +
            "LEFT JOIN FETCH p.parameterOptions po " +
            "WHERE p.active = TRUE AND po.active = TRUE " +
            "ORDER BY p.rank ASC, po.rank ASC")
    List<Parameter> listActive();
}
