package no.glassverden.parameter.repository;

import no.glassverden.parameter.model.ParameterOption;
import no.glassverden.parameter.model.ParameterOptionType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParameterOptionRepository extends JpaRepository<ParameterOption, ParameterOptionType> {

}
