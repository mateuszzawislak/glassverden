package no.glassverden.parameter.controller;

import no.glassverden.parameter.dto.ParameterDto;
import no.glassverden.parameter.serializer.ParameterSerializer;
import no.glassverden.parameter.service.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/parameters")
public class ParameterApiController {

    private ParameterService parameterService;

    @Autowired
    public ParameterApiController(ParameterService parameterService) {
        this.parameterService = parameterService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ParameterDto> list() {
        return parameterService.listActive().stream()
                .map(ParameterSerializer::toDto)
                .collect(Collectors.toList());
    }
}
