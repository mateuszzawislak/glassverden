package no.glassverden.parameter.deserializer;

import no.glassverden.parameter.dto.ParameterOptionDto;
import no.glassverden.parameter.model.ParameterOption;

public class ParameterDeserializer {

    public static ParameterOption fromDto(ParameterOptionDto dto) {
        ParameterOption parameterOption = new ParameterOption();

        parameterOption.setCode(dto.getCode());

        return parameterOption;
    }
}
