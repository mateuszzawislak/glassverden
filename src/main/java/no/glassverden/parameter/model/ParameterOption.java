package no.glassverden.parameter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "parameter_option")
public class ParameterOption {

    @Id
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ParameterOptionType code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parameter_code", nullable = false)
    private Parameter parameter;

    @Column(nullable = false)
    private int rank;

    @Column(nullable = false, name = "price_weight", precision = 15, scale = 5)
    private BigDecimal priceWeight;

    @Column(nullable = false)
    private boolean active;

    public ParameterOptionType getCode() {
        return code;
    }

    public void setCode(ParameterOptionType code) {
        this.code = code;
    }

    public Parameter getParameter() {
        return parameter;
    }

    public int getRank() {
        return rank;
    }

    public BigDecimal getPriceWeight() {
        return priceWeight;
    }

    public boolean isActive() {
        return active;
    }
}
