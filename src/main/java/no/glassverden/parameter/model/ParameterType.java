package no.glassverden.parameter.model;

public enum ParameterType {
    COLOR,
    GLASS_COLOR,
    GLASS_THICKNESS,
    GLASS_TYPE,
    HANDRAIL,
    HEIGHT,
    MOUNTING,
    MOUNTING_END,
    MOUNTING_START,
    PROFILE,
    STEEL_CLASS;
}
