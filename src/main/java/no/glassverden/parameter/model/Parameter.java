package no.glassverden.parameter.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "parameter")
public class Parameter {

    @Id
    @Enumerated(EnumType.STRING)
    private ParameterType code;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parameter")
    @OrderBy("rank")
    private Set<ParameterOption> parameterOptions;

    @Column(nullable = false)
    private int rank;

    @Column(nullable = false)
    private boolean active;

    public ParameterType getCode() {
        return code;
    }

    public Set<ParameterOption> getParameterOptions() {
        return parameterOptions;
    }

    public int getRank() {
        return rank;
    }

    public boolean isActive() {
        return active;
    }
}
