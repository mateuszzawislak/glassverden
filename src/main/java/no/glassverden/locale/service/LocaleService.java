package no.glassverden.locale.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Locale;

@Service
public class LocaleService {

    private MessageSource messageSource;

    private MessageSourceAccessor accessor;

    @Autowired
    private LocaleService(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @PostConstruct
    private void init() {
        accessor = new MessageSourceAccessor(messageSource);
    }

    public String getMessage(String code) {
        return accessor.getMessage(code, LocaleContextHolder.getLocale());
    }

    public String getMessage(String code, Locale locale) {
        return accessor.getMessage(code, locale);
    }
}
