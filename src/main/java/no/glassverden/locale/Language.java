package no.glassverden.locale;

import java.util.Arrays;
import java.util.Locale;

public enum Language {
    EN("en"),
    NO("no"),
    PL("pl");

    public static Language DEFAULT = NO;
    public static Locale DEFAULT_LOCALE = new Locale(DEFAULT.getCode());

    private String code;

    Language(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static Language getByCode(String code) {
        return Arrays.asList(values()).stream()
                .filter(language -> language.code.equals(code))
                .findAny()
                .orElse(DEFAULT);
    }
}
