package no.glassverden.locale;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import no.glassverden.web.WebConstants;
import java.nio.charset.StandardCharsets;

@Configuration
public class LocaleConfiguration implements WebMvcConfigurer {

    private static final String MESSAGES_BASENAME = "classpath:i18n/messages";
    private static final String LABELS_BASENAME = "classpath:i18n/labels";

    @Bean
    public LocaleResolver localeResolver() {
        CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        cookieLocaleResolver.setCookieName(WebConstants.LOCALE_COOKIE_NAME);
        cookieLocaleResolver.setCookieMaxAge(WebConstants.LOCALE_COOKIE_MAX_AGE);
        cookieLocaleResolver.setDefaultLocale(Language.DEFAULT_LOCALE);

        return cookieLocaleResolver;
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(MESSAGES_BASENAME, LABELS_BASENAME);
        messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
        return messageSource;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName(WebConstants.LOCALE_PARAM_NAME);
        return lci;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }
}
