package no.glassverden.project.dto;

import static no.glassverden.web.validation.ValidationErrorMessage.NOT_EMPTY;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import no.glassverden.parameter.dto.ParameterOptionDto;
import no.glassverden.project.model.ProjectStatus;
import no.glassverden.project.model.ProjectType;
import no.glassverden.project.validation.ProjectUpdateValidationGroup;
import no.glassverden.transaction.dto.ContactDto;
import no.glassverden.transaction.dto.TransactionDto;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class ProjectDto {

    private String code;

    private ProjectStatus status;

    private ProjectType type;

    @Valid
    @NotEmpty(message = NOT_EMPTY, groups = ProjectUpdateValidationGroup.class)
    private List<EdgeDto> edges;

    @Valid
    @NotEmpty(message = NOT_EMPTY, groups = ProjectUpdateValidationGroup.class)
    private List<ParameterOptionDto> parameterOptions;

    private List<TransactionDto> transactions;

    private ContactDto contact;

    private BigDecimal netPrice;

    private BigDecimal grossPrice;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public ProjectType getType() {
        return type;
    }

    public void setType(ProjectType type) {
        this.type = type;
    }

    public List<EdgeDto> getEdges() {
        return edges;
    }

    public void setEdges(List<EdgeDto> edges) {
        this.edges = edges;
    }

    public List<ParameterOptionDto> getParameterOptions() {
        return parameterOptions;
    }

    public void setParameterOptions(List<ParameterOptionDto> parameterOptions) {
        this.parameterOptions = parameterOptions;
    }

    public List<TransactionDto> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionDto> transactions) {
        this.transactions = transactions;
    }

    public ContactDto getContact() {
        return contact;
    }

    public void setContact(ContactDto contact) {
        this.contact = contact;
    }

    public BigDecimal getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(BigDecimal netPrice) {
        this.netPrice = netPrice;
    }

    public BigDecimal getGrossPrice() {
        return grossPrice;
    }

    public void setGrossPrice(BigDecimal grossPrice) {
        this.grossPrice = grossPrice;
    }
}
