package no.glassverden.project.dto;

import static no.glassverden.web.validation.ValidationErrorMessage.MAX_359;
import static no.glassverden.web.validation.ValidationErrorMessage.MIN_0;
import static no.glassverden.web.validation.ValidationErrorMessage.MIN_1;
import static no.glassverden.web.validation.ValidationErrorMessage.NOT_NULL;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import no.glassverden.project.validation.ProjectUpdateValidationGroup;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class EdgeDto {

    private Integer id;

    @Min(value = 1, message = MIN_1, groups = ProjectUpdateValidationGroup.class)
    @Max(value = 359, message = MAX_359, groups = ProjectUpdateValidationGroup.class)
    private Integer angle;

    @NotNull(message = NOT_NULL, groups = ProjectUpdateValidationGroup.class)
    @Min(value = 1, message = MIN_1, groups = ProjectUpdateValidationGroup.class)
    private Integer segmentsNumber;

    @NotNull(message = NOT_NULL, groups = ProjectUpdateValidationGroup.class)
    @Min(value = 1, message = MIN_1, groups = ProjectUpdateValidationGroup.class)
    private Integer length;

    @NotNull(message = NOT_NULL, groups = ProjectUpdateValidationGroup.class)
    @Min(value = 0, message = MIN_0, groups = ProjectUpdateValidationGroup.class)
    private Integer rank;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAngle() {
        return angle;
    }

    public void setAngle(Integer angle) {
        this.angle = angle;
    }

    public Integer getSegmentsNumber() {
        return segmentsNumber;
    }

    public void setSegmentsNumber(Integer segmentsNumber) {
        this.segmentsNumber = segmentsNumber;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }
}
