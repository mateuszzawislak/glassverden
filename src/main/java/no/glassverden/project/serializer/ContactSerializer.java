package no.glassverden.project.serializer;

import no.glassverden.project.model.Contact;
import no.glassverden.transaction.dto.ContactDto;

public class ContactSerializer {

    public static ContactDto toDto(Contact contact) {
        ContactDto dto = new ContactDto();

        dto.setEmail(contact.getEmail());
        dto.setPhone(contact.getPhone());
        dto.setName(contact.getName());
        dto.setSurname(contact.getSurname());
        dto.setPostalCode(contact.getPostalCode());
        dto.setCity(contact.getCity());
        dto.setStreet(contact.getStreet());
        dto.setFlatNumber(contact.getFlatNumber());

        return dto;
    }
}
