package no.glassverden.project.serializer;

import java.util.stream.Collectors;

import no.glassverden.parameter.serializer.ParameterSerializer;
import no.glassverden.project.dto.EdgeDto;
import no.glassverden.project.dto.ProjectDto;
import no.glassverden.project.model.Edge;
import no.glassverden.project.model.Project;
import no.glassverden.transaction.serializer.TransactionSerializer;

public class ProjectSerializer {

    public static ProjectDto toDto(Project project) {
        ProjectDto projectDto = new ProjectDto();

        projectDto.setCode(project.getCode());
        projectDto.setStatus(project.getStatus());
        projectDto.setType(project.getType());

        projectDto.setEdges(project.getEdges().stream()
                .map(ProjectSerializer::toDto)
                .collect(Collectors.toList()));

        projectDto.setParameterOptions(project.getParameterOptions().stream()
                .map(ParameterSerializer::toDto)
                .collect(Collectors.toList()));

        if (project.getTransactions() != null) {
            projectDto.setTransactions(project.getTransactions().stream()
                    .map(TransactionSerializer::toDto)
                    .collect(Collectors.toList()));
        }

        if (project.getContact() != null) {
            projectDto.setContact(ContactSerializer.toDto(project.getContact()));
        }

        return projectDto;
    }

    private static EdgeDto toDto(Edge edge) {
        EdgeDto dto = new EdgeDto();

        dto.setId(edge.getId());
        dto.setAngle(edge.getAngle());
        dto.setLength(edge.getLength());
        dto.setRank(edge.getRank());
        dto.setSegmentsNumber(edge.getSegmentsNumber());

        return dto;
    }
}
