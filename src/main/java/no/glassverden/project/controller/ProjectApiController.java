package no.glassverden.project.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import no.glassverden.project.deserializer.ProjectDeserializer;
import no.glassverden.project.dto.ProjectDto;
import no.glassverden.project.model.Project;
import no.glassverden.project.model.ProjectType;
import no.glassverden.project.serializer.ProjectSerializer;
import no.glassverden.project.service.ProjectBusinessDelegate;
import no.glassverden.project.service.ProjectService;
import no.glassverden.project.validation.ProjectUpdateValidationGroup;

@RestController
@RequestMapping("api/projects")
public class ProjectApiController {

    private ProjectService projectService;

    private ProjectBusinessDelegate projectBusinessDelegate;

    @Autowired
    public ProjectApiController(ProjectService projectService,
                                ProjectBusinessDelegate projectBusinessDelegate) {
        this.projectService = projectService;
        this.projectBusinessDelegate = projectBusinessDelegate;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ProjectDto> create(
            @RequestParam(name = "type", required = false, defaultValue = ProjectType.DEFAULT_NAME) ProjectType projectType,
            HttpServletRequest request) {

        return createResponse(projectService.create(projectType, request.getRemoteAddr()));
    }

    @RequestMapping(path = "{code}", method = RequestMethod.GET)
    public ResponseEntity<ProjectDto> get(@PathVariable String code) {
        return createResponse(projectService.get(code));
    }

    @RequestMapping(path = "{code}", method = RequestMethod.PUT)
    public ResponseEntity<ProjectDto> update(@PathVariable String code,
                                             @RequestBody @Validated(ProjectUpdateValidationGroup.class) ProjectDto projectDto) {
        return createResponse(projectService.update(code, ProjectDeserializer.fromDto(projectDto)));
    }

    @RequestMapping(path = "{code}/reopen", method = RequestMethod.PUT)
    public ResponseEntity<ProjectDto> reopen(@PathVariable String code) {
        return createResponse(projectService.reopen(code));
    }

    private ResponseEntity<ProjectDto> createResponse(Project project) {
        ProjectDto dto = ProjectSerializer.toDto(project);
        projectBusinessDelegate.fillPrices(dto, project);
        return ResponseEntity.ok(dto);
    }

}
