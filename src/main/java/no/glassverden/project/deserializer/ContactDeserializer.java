package no.glassverden.project.deserializer;

import no.glassverden.project.model.Contact;
import no.glassverden.transaction.dto.ContactDto;

public class ContactDeserializer {

    public static Contact fromDto(ContactDto dto) {
        Contact contact = new Contact();

        contact.setEmail(dto.getEmail());
        contact.setPhone(dto.getPhone());
        contact.setName(dto.getName());
        contact.setSurname(dto.getSurname());
        contact.setPostalCode(dto.getPostalCode());
        contact.setCity(dto.getCity());
        contact.setStreet(dto.getStreet());
        contact.setFlatNumber(dto.getFlatNumber());

        return contact;
    }
}
