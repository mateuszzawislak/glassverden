package no.glassverden.project.deserializer;

import no.glassverden.parameter.deserializer.ParameterDeserializer;
import no.glassverden.project.dto.EdgeDto;
import no.glassverden.project.dto.ProjectDto;
import no.glassverden.project.model.Edge;
import no.glassverden.project.model.Project;

import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class ProjectDeserializer {

    public static Project fromDto(ProjectDto dto) {
        Project project = new Project();

        project.setCode(dto.getCode());

        project.setEdges(dto.getEdges().stream()
                .map(edge -> ProjectDeserializer.fromDto(edge, project))
                .collect(Collectors.toCollection(LinkedHashSet::new)));

        project.setParameterOptions(dto.getParameterOptions().stream()
                .map(ParameterDeserializer::fromDto)
                .collect(Collectors.toCollection(LinkedHashSet::new)));

        return project;
    }

    private static Edge fromDto(EdgeDto dto, Project project) {
        Edge edge = new Edge();

        if (dto.getId() != null) {
            edge.setId(dto.getId());
        }
        edge.setAngle(dto.getAngle());
        edge.setLength(dto.getLength());
        edge.setRank(dto.getRank());
        edge.setSegmentsNumber(dto.getSegmentsNumber());
        edge.setProject(project);

        return edge;
    }
}
