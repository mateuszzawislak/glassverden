package no.glassverden.project.validation;

import java.util.Collections;
import java.util.Optional;

import no.glassverden.core.validation.Validator;
import no.glassverden.project.model.Project;
import no.glassverden.web.validation.ValidationErrorMessage;
import no.glassverden.web.validation.exception.IllegalInputException;

public class SingleSegmentLengthValidator implements Validator<Project> {

    private static final int SEGMENT_MIN_LENGTH = 800; // [mm]
    private static final int SEGMENT_MAX_LENGTH = 1200; // [mm]

    @Override
    public void validate(Project project) {
        Optional.ofNullable(project.getEdges()).orElse(Collections.emptySet()).stream().forEach(edge -> {
            int segmentLength = edge.getLength() / edge.getSegmentsNumber();

            if (edge.getLength() < SEGMENT_MIN_LENGTH && edge.getSegmentsNumber() != 1) {
                throw new IllegalInputException(ValidationErrorMessage.ONE_SEGMENT_REQUIRED);
            }

            if (edge.getLength() > SEGMENT_MIN_LENGTH && segmentLength < SEGMENT_MIN_LENGTH) {
                throw new IllegalInputException(ValidationErrorMessage.SEGMENT_TOO_SHORT);
            }

            if (segmentLength > SEGMENT_MAX_LENGTH) {
                throw new IllegalInputException(ValidationErrorMessage.SEGMENT_TOO_LONG);
            }
        });
    }

}
