package no.glassverden.project.validation;

import java.util.Collections;
import java.util.Optional;

import no.glassverden.core.validation.Validator;
import no.glassverden.project.model.Project;
import no.glassverden.web.validation.ValidationErrorMessage;
import no.glassverden.web.validation.exception.IllegalInputException;

public class EdgeLengthValidator implements Validator<Project> {

    private static final int EDGE_MIN_LENGTH = 300; // [mm]
    private static final int EDGE_MAX_LENGTH = 18000; // [mm]

    @Override
    public void validate(Project project) {
        Optional.ofNullable(project.getEdges()).orElse(Collections.emptySet()).stream().forEach(edge -> {
            int edgeLength = edge.getLength();

            if (edgeLength < EDGE_MIN_LENGTH) {
                throw new IllegalInputException(ValidationErrorMessage.EDGE_TOO_SHORT);
            }

            if (edgeLength > EDGE_MAX_LENGTH) {
                throw new IllegalInputException(ValidationErrorMessage.EDGE_TOO_LONG);
            }
        });
    }

}
