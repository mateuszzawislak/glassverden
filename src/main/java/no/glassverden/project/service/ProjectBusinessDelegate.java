package no.glassverden.project.service;

import no.glassverden.project.dto.ProjectDto;
import no.glassverden.project.model.Project;
import no.glassverden.project.service.price.PriceCalculationStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectBusinessDelegate {

    private PriceCalculationStrategy priceCalculationStrategy;

    @Autowired
    public ProjectBusinessDelegate(PriceCalculationStrategy priceCalculationStrategy) {
        this.priceCalculationStrategy = priceCalculationStrategy;
    }

    public void fillPrices(ProjectDto dto, Project project) {
        dto.setNetPrice(priceCalculationStrategy.getNetPrice(project));
        dto.setGrossPrice(priceCalculationStrategy.getGrossPrice(project));
    }
}
