package no.glassverden.project.service;

import static no.glassverden.web.validation.ValidationErrorMessage.INVALID_PROJECT_STATUS;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import no.glassverden.core.validation.Validator;
import no.glassverden.locale.Language;
import no.glassverden.parameter.model.ParameterOption;
import no.glassverden.parameter.model.ParameterOptionType;
import no.glassverden.parameter.repository.ParameterOptionRepository;
import no.glassverden.parameter.service.ParameterService;
import no.glassverden.project.defaults.ProjectsFactory;
import no.glassverden.project.model.Edge;
import no.glassverden.project.model.Project;
import no.glassverden.project.model.ProjectStatus;
import no.glassverden.project.model.ProjectType;
import no.glassverden.project.repository.ProjectRepository;
import no.glassverden.project.validation.EdgeLengthValidator;
import no.glassverden.project.validation.SingleSegmentLengthValidator;
import no.glassverden.transaction.service.TransactionService;
import no.glassverden.web.validation.ValidationErrorMessage;
import no.glassverden.web.validation.exception.IllegalInputException;
import no.glassverden.web.validation.exception.ObjectNotFoundException;
import no.glassverden.web.validation.exception.UnsupportedOperationException;

@Service
public class ProjectService {

    private static final Set<ProjectStatus> PROJECT_NOT_PAID_STATUSES =
            new HashSet<>(Arrays.asList(ProjectStatus.NEW, ProjectStatus.COMPLETED));

    private static final List<Validator<Project>> PROJECT_VALIDATORS = new ArrayList<>();

    static {
        PROJECT_VALIDATORS.add(new EdgeLengthValidator());
        PROJECT_VALIDATORS.add(new SingleSegmentLengthValidator());
    }

    private ProjectRepository projectRepository;

    private ProjectsFactory projectsFactory;

    private ParameterOptionRepository parameterOptionRepository;

    private ParameterService parameterService;

    private TransactionService transactionService;

    @Autowired
    public ProjectService(ProjectRepository projectRepository,
                          ProjectsFactory projectsFactory,
                          ParameterOptionRepository parameterOptionRepository,
                          ParameterService parameterService,
                          @Lazy TransactionService transactionService) {
        this.projectRepository = projectRepository;
        this.projectsFactory = projectsFactory;
        this.parameterOptionRepository = parameterOptionRepository;
        this.parameterService = parameterService;
        this.transactionService = transactionService;
    }

    public Project create(ProjectType projectType, String ip) {
        Project project = projectsFactory.getProject(projectType);

        project.setCode(UUID.randomUUID().toString());
        project.setStatus(ProjectStatus.NEW);
        project.setType(projectType);
        project.setLanguage(Language.getByCode(LocaleContextHolder.getLocale().getLanguage()));
        project.setIp(ip);

        return projectRepository.save(project);
    }

    public Project get(String code) {
        return projectRepository.getByCodeWithEdgesAndOptionsAndTransactionsAndContact(code)
                .orElseThrow(() -> new ObjectNotFoundException(ValidationErrorMessage.PROJECT_NOT_FOUND));
    }

    public Project update(String code, Project project) {
        if (!code.equals(project.getCode())) {
            throw new ObjectNotFoundException(ValidationErrorMessage.PROJECT_NOT_FOUND);
        }

        Project dbProject = get(code);

        if (!ProjectStatus.NEW.equals(dbProject.getStatus())) {
            throw new IllegalInputException(ValidationErrorMessage.INVALID_PROJECT_STATUS);
        }

        validateProject(project);

        mergeEdges(dbProject, project);
        mergeParameterOptions(dbProject, project);
        dbProject = projectRepository.save(dbProject);

        return get(dbProject.getCode());
    }

    public Project reopen(String projectCode) {
        Project project = get(projectCode);
        validateAndChangeProjectStatus(project, ProjectStatus.NEW);
        transactionService.validateAndCloseExistingTransactions(project);
        project = projectRepository.save(project);

        return get(project.getCode());
    }

    public void validateAndChangeProjectStatus(Project project, ProjectStatus statusToSet) {
        if (!PROJECT_NOT_PAID_STATUSES.contains(project.getStatus())) {
            throw new UnsupportedOperationException(INVALID_PROJECT_STATUS);
        }
        project.setStatus(statusToSet);
    }

    private void mergeEdges(Project dbProject, Project project) {
        Set<Integer> existingEdges = dbProject.getEdges().stream()
                .map(Edge::getId)
                .collect(Collectors.toSet());

        project.getEdges().stream()
                .forEach(edge -> {
                    if (!existingEdges.contains(edge.getId())) {
                        edge.setId(0);
                    }
                });

        dbProject.setEdges(project.getEdges());
    }

    private void mergeParameterOptions(Project dbProject, Project project) {
        Set<ParameterOptionType> activeOptionTypes = parameterService.listActiveOptionTypes();

        dbProject.setParameterOptions(project.getParameterOptions().stream()
                .map(ParameterOption::getCode)
                .map(selectedOption -> {
                    if (!activeOptionTypes.contains(selectedOption)) {
                        throw new ObjectNotFoundException(ValidationErrorMessage.PARAMETER_NOT_FOUND);
                    }
                    return parameterOptionRepository.getOne(selectedOption);
                })
                .collect(Collectors.toCollection(LinkedHashSet::new)));
    }

    private void validateProject(Project project) {
        for (Validator<Project> validator : PROJECT_VALIDATORS) {
            validator.validate(project);
        }
    }

}
