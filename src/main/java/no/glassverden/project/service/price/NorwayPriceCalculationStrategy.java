package no.glassverden.project.service.price;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import no.glassverden.parameter.model.ParameterOption;
import no.glassverden.project.model.Edge;
import no.glassverden.project.model.Project;

@Service
public class NorwayPriceCalculationStrategy implements PriceCalculationStrategy {

    @Value("${price.norway.tax}")
    private BigDecimal tax;

    @Value("${price.norway.base.price}")
    private BigDecimal basePrice;

    @Value("#{${price.norway.delivery.price}}")
    TreeMap<Integer, BigDecimal> deliveryCosts;

    @Override
    public BigDecimal getNetPrice(Project project) {
        BigDecimal deliveryPrice = getDeliveryPrice(project);
        BigDecimal meterialsPrice =
                calculatePricePerMeter(project).multiply(BigDecimal.valueOf(calculateTotalLength(project)));

        return roundPrice(deliveryPrice.add(meterialsPrice));
    }

    @Override
    public BigDecimal getGrossPrice(Project project) {
        BigDecimal netPrice = getNetPrice(project);
        BigDecimal taxValue = netPrice.multiply(getTax(project));

        return roundPrice(netPrice.add(taxValue));
    }

    @Override
    public BigDecimal getDeliveryPrice(Project project) {
        int totalLength = calculateTotalLength(project);
        SortedSet<Integer> minimalLengthKeys = new TreeSet(deliveryCosts.keySet());
        BigDecimal result = null;

        for (Integer minimalLength : minimalLengthKeys) {
            if (totalLength >= minimalLength) {
                result = deliveryCosts.get(minimalLength);
            }
        }

        return result;
    }

    @Override
    public BigDecimal getParameterOptionPriceWeight(ParameterOption parameterOption) {
        return parameterOption.getPriceWeight();
    }

    @Override
    public BigDecimal getTax(Project project) {
        return tax;
    }

    private BigDecimal calculatePricePerMeter(Project project) {
        BigDecimal priceFromSelectedOptions = project.getParameterOptions().stream()
                .map(ParameterOption::getPriceWeight)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return basePrice.add(priceFromSelectedOptions);
    }

    private int calculateTotalLength(Project project) {
        return project.getEdges().stream()
                .mapToInt(Edge::getLength)
                .sum();
    }

    private BigDecimal roundPrice(BigDecimal price) {
        return price.setScale(0, RoundingMode.UP);
    }
}
