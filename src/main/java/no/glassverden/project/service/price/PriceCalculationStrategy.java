package no.glassverden.project.service.price;

import no.glassverden.parameter.model.ParameterOption;
import no.glassverden.project.model.Project;

import java.math.BigDecimal;

public interface PriceCalculationStrategy {

    BigDecimal getNetPrice(Project project);

    BigDecimal getGrossPrice(Project project);

    BigDecimal getDeliveryPrice(Project project);

    BigDecimal getParameterOptionPriceWeight(ParameterOption parameterOption);

    BigDecimal getTax(Project project);
}
