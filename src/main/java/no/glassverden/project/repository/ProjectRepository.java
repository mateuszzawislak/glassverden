package no.glassverden.project.repository;

import no.glassverden.project.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ProjectRepository extends JpaRepository<Project, String> {

    @Query("SELECT p FROM Project p " +
            "LEFT JOIN FETCH p.edges " +
            "LEFT JOIN FETCH p.parameterOptions " +
            "LEFT JOIN FETCH p.transactions " +
            "LEFT JOIN FETCH p.contact " +
            "WHERE p.code = :code")
    Optional<Project> getByCodeWithEdgesAndOptionsAndTransactionsAndContact(@Param("code") String code);
}
