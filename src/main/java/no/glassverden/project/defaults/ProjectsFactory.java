package no.glassverden.project.defaults;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import no.glassverden.parameter.model.ParameterOptionType;
import no.glassverden.parameter.repository.ParameterOptionRepository;
import no.glassverden.project.model.Edge;
import no.glassverden.project.model.Project;
import no.glassverden.project.model.ProjectType;

@Service
public class ProjectsFactory {

    private ParameterOptionRepository parameterOptionRepository;

    private static Map<ProjectType, Set<ParameterOptionType>> parameterOptions;

    static {
        parameterOptions = new HashMap<>();

        Set<ParameterOptionType> railingWithPosts = new HashSet<>();
        railingWithPosts.add(ParameterOptionType.HANDRAIL_WITH_RAIL);
        railingWithPosts.add(ParameterOptionType.GLASS_NORMAL);
        railingWithPosts.add(ParameterOptionType.GLASS_COLOR_TRANSPARENT);
        railingWithPosts.add(ParameterOptionType.GLASS_THICKNESS_44_2);
        railingWithPosts.add(ParameterOptionType.PROFILE_ROUND);
        railingWithPosts.add(ParameterOptionType.COLOR_SHINY);
        railingWithPosts.add(ParameterOptionType.MOUNTING_GROUND);
        railingWithPosts.add(ParameterOptionType.MOUNTING_END_HANDRAIL);
        railingWithPosts.add(ParameterOptionType.MOUNTING_START_HANDRAIL);
        railingWithPosts.add(ParameterOptionType.HEIGHT_1000);
        railingWithPosts.add(ParameterOptionType.STEEL_CLASS_AISI316);
        parameterOptions.put(ProjectType.RAILING_WITH_POSTS, railingWithPosts);

        Set<ParameterOptionType> railingWithBar = new HashSet<>();
        //TODO set correct defaults
        railingWithBar.add(ParameterOptionType.GLASS_TEMPERED);
        railingWithBar.add(ParameterOptionType.HANDRAIL_WITH_RAIL);
        parameterOptions.put(ProjectType.RAILING_WITH_BAR, railingWithBar);

        Set<ParameterOptionType> railingWithRotules = new HashSet<>();
        //TODO set correct defaults
        railingWithRotules.add(ParameterOptionType.GLASS_TEMPERED);
        railingWithRotules.add(ParameterOptionType.HANDRAIL_WITH_RAIL);
        parameterOptions.put(ProjectType.RAILING_WITH_ROTULES, railingWithRotules);
    }

    @Autowired
    public ProjectsFactory(ParameterOptionRepository parameterOptionRepository) {
        this.parameterOptionRepository = parameterOptionRepository;
    }

    public Project getProject(ProjectType projectType) {
        Project project = new Project();

        Set<Edge> edges = new HashSet<>();
        edges.add(createEdge(0, 1, 1000, project, null));
        edges.add(createEdge(1, 3, 3000, project, 90));
        edges.add(createEdge(2, 1, 1000, project, 90));
        project.setEdges(edges);

        project.setParameterOptions(new HashSet<>());
        getParameters(projectType).stream().forEach(parameterOptionType ->
                project.getParameterOptions().add(parameterOptionRepository.findById(parameterOptionType).get()));

        return project;
    }

    private Edge createEdge(int rank, int segmentsNumber, int length, Project project, Integer angle) {
        Edge edge = new Edge();

        edge.setRank(rank);
        edge.setSegmentsNumber(segmentsNumber);
        edge.setLength(length);
        edge.setProject(project);
        edge.setAngle(angle);

        return edge;
    }

    @SuppressWarnings("unchecked")
    private Set<ParameterOptionType> getParameters(ProjectType projectType) {
        return ProjectsFactory.parameterOptions.getOrDefault(projectType, Collections.EMPTY_SET);
    }
}
