package no.glassverden.project.defaults;

public enum ProjectType {

    RAILING_WITH_BAR,
    RAILING_WITH_POSTS,
    RAILING_WITH_ROTULES;

    public static ProjectType DEFAULT = RAILING_WITH_POSTS;

}
