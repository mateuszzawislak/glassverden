package no.glassverden.project.model;

public enum ProjectStatus {
    NEW,
    COMPLETED,
    PAID,
    FINISHED;
}
