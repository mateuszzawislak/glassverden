package no.glassverden.project.model;

public enum ProjectType {

    RAILING_WITH_BAR,
    RAILING_WITH_POSTS,
    RAILING_WITH_ROTULES;

    public static final String DEFAULT_NAME = "RAILING_WITH_POSTS";

}
