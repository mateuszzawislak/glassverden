package no.glassverden;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class GlassverdenApplication {

	public static void main(String[] args) {
		SpringApplication.run(GlassverdenApplication.class, args);
	}

}
